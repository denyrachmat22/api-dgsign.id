<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRsvpUsersDetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rsvp_users_det', function (Blueprint $table) {
            $table->id();
            $table->string('username')->unique();
            $table->string('users_det_addr1')->nullable();
            $table->string('users_det_addr2')->nullable();
            $table->string('users_det_phone')->nullable();
            $table->integer('users_det_fam_count')->nullable();
            $table->integer('users_det_souvenir_count')->nullable();
            $table->integer('users_det_angpau_count')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rsvp_users_det');
    }
}
