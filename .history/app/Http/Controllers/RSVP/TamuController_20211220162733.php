<?php

namespace App\Http\Controllers\RSVP;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

// Model
use App\Models\User;
use App\Models\UserDet;

class TamuController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth:api', ['except' => ['login','register', 'registerTamu', 'loginViaQR']]);
    }

    public function searchTamu(Request $req)
    {
        //validate incoming request
        $this->validate($req, [
            'query_str' => 'required|string'
        ]);

        try {
            $cariUser = UserDet::where('fullname', 'like', '%'.$req->query_str.'%')->users->get()->toArray();

            if (count($cariUser) > 0) {
                return [
                    'status' => true,
                    'label' => 'User found !',
                    'data' => $cariUser
                ];
            } else {
                return [
                    'status' => false,
                    'label' => 'User not found !',
                    'data' => $cariUser
                ];
            }
        } catch (\Throwable $th) {
            return response()->json( [
                'status' => false,
                'label' => $th->getMessage(),
                'data' => [
                    'entity' => 'search_tamu',
                    'action' => 'create',
                    'result' => $th->getMessage()
                ]
            ], 409);
        }
    }
}
