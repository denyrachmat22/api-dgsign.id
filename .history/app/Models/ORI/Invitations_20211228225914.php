<?php

namespace App\Models\ORI;

use Illuminate\Database\Eloquent\Model;

class Invitations extends Model {
    protected $connection = 'mysql_db_old';
    protected $table = 'invitations';
    public $timestamps = false;
    protected $fillable = [
        'ivts_Id',
        'ivts_Client_Id',
        'ivts_Uuid',
        'ivts_Name',
        'ivts_NoHp',
        'ivts_Address',
        'ivts_Guest',
        'ivts_GuestAtt',
        'ivts_GuestAttTime',
        'ivts_GuestAttCounter',
        'ivts_Souvenir',
        'ivts_SouvenirAtt',
        'ivts_SouvenirAttTime',
        'ivts_SouvenirAttCounter',
        'ivts_Angpau',
        'ivts_AngpauAtt',
        'ivts_AngpauAttTime',
        'ivts_AngpauAttCounter',
        'ivts_Category',
        'ivts_Group',
        'ivts_GroupFam',
        'ivts_GroupSub',
        'ivts_Seat',
        'ivts_Selfie',
        'ivts_SelfieFile',
        'ivts_SelfieTime',
        'ivts_capt_Id',
        'ivts_tplqr_Id',
        'ivts_tplivts_Id',
        'ivts_CounterAtt',
        'ivts_Files',
        'ivts_FilesIvts',
        'ivts_RsvpStatus',
        'ivts_RsvpGuest',
        'ivts_RsvpSendCount',
        'ivts_RsvpRespond',
        'ivts_RsvpMessage',
        'ivts_RsvpTime',
        'ivts_LinkExternal',
        'ivts_SentTime',
        'ivts_SentStatus',
        'ivts_Created',
        'ivts_Updated',
        'ivts_Deleted'
    ];
}
