<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

// Models
use App\Models\Settings\Role;
use App\Models\Settings\RoleDet;
use App\Models\Settings\RoleUser;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login','register']]);
    }

    public function store(Request $req)
    {
        $this->validate($req, [
            'roles_name' => 'required|unique:rsvp_roles_mstr,roles_name'
        ]);

        try {
            $insert = Role::create($req->all());

            return [
                'status' => true,
                'label' => 'Menu successfully inserted !',
                'data' => $insert
            ];
        } catch (\Throwable $th) {
            return response()->json( [
                'entity' => 'rsvp_roles_mstr',
                'action' => 'create',
                'result' => $th->getMessage()
            ], 409);
        }
    }

    public function storeDet(Request $req)
    {
        $this->validate($req, [
            'roles_mstr_id' => 'required',
            'menus_id' => 'required|array|check_array:1'
        ]);

        try {
            $insert = [];
            foreach ($req->menus_id as $key => $value) {
                $insert[] = RoleDet::create([
                    'roles_mstr_id' => $req->roles_mstr_id,
                    'menus_id' => $value
                ]);
            }

            return [
                'status' => true,
                'label' => 'Menu successfully inserted !',
                'data' => $insert
            ];
        } catch (\Throwable $th) {
            return response()->json( [
                'entity' => 'rsvp_roles_det',
                'action' => 'create',
                'result' => $th->getMessage()
            ], 409);
        }
    }

    public function storeUserMapping(Request $req)
    {
        $this->validate($req, [
            'username' => 'required|unique:rsvp_roles_mstr,roles_name'
        ]);

        try {
            $insert = RoleUser::create($req->all());

            return [
                'status' => true,
                'label' => 'Menu successfully inserted !',
                'data' => $insert
            ];
        } catch (\Throwable $th) {
            return response()->json( [
                'entity' => 'rsvp_user_roles',
                'action' => 'create',
                'result' => $th->getMessage()
            ], 409);
        }
    }
}
