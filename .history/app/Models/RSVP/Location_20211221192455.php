<?php

namespace App\Models\RSVP;

use Illuminate\Database\Eloquent\Model;

class Location extends Model {
    protected $table = 'rsvp_loc_mstr';
    protected $fillable = [
        'loc_name',
        'loc_desc',
        'loc_parent',
        'loc_capa',
    ];
}
