<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserDet extends Model {
    protected $primaryKey = 'username';
    protected $keyType = 'string';
    protected $table = 'rsvp_users_det';
    protected $fillable = [
        'username',
        'fullname',
        'users_det_addr1',
        'users_det_addr2',
        'users_det_phone',
        'users_det_fam_count',
        'users_det_souvenir_count',
        'users_det_angpau_count',
        'loc_id'
    ];

    public function users()
    {
        return $this->belongsTo('App\Models\User','username','username');
    }

    public function category()
    {
        return $this->hasMany('App\Models\Settings\UserCategory','username','username');
    }
}
