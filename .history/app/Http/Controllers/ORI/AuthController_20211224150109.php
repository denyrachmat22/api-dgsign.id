<?php

namespace App\Http\Controllers\ORI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

// Model
use App\Models\ORI\User;

class AuthController extends Controller
{
    /**
        * @OA\Post(
        *     path="/api/login",
        *     operationId="/api/auth/login",
        *     tags={"Invitations"},
        *     @OA\Parameter(
        *         name="username",
        *         in="query",
        *         description="Username user for login",
        *         required=true,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Parameter(
        *         name="password",
        *         in="query",
        *         description="Password user for login",
        *         required=true,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Response(
        *         response="200",
        *         description="Returns authorization token for login token",
        *         @OA\JsonContent(
        *              type="object",
        *              @OA\Property(property="status", type="boolean"),
        *              @OA\Property(property="data", type="array", @OA\Items()),
        *              @OA\Property(property="token", type="string"),
        *              @OA\Property(property="token_type", type="string")
        *         )
        *     ),
        *     @OA\Response(
        *         response="401",
        *         description="Error: username or password not right",
        *          @OA\JsonContent(
        *              type="object",
        *              @OA\Property(property="status", type="boolean"),
        *              @OA\Property(property="message", type="string"),
        *         )
        *     ),
        * )
    */
    public function login(Request $req)
    {
         //validate incoming request
         $this->validate($req, [
            'username' => 'required|string',
            'password' => 'required|string',
        ]);

        try {
            $dataUser = User::where('admnUsername', $req->username)->first();
            if(!empty($dataUser)) {
                if (password_verify($req->password, $dataUser->admnPassword)) {
                    return response()->json([
                        'status' => true,
                        'message' => 'Login successfull !',
                        'data' => $dataUser
                    ], 200);
                }

                return response()->json([
                    'status' => false,
                    'message' => 'Unauthorized'
                ], 401);
            } else {
                return 'not_found';
            }
        } catch (\Throwable $th) {
            return response()->json( [
                'status' => false,
                'label' => $th->getMessage(),
                'data' => [
                    'entity' => 'administrator',
                    'action' => 'create',
                    'result' => $th->getMessage()
                ]
            ], 409);
        }
    }
}
