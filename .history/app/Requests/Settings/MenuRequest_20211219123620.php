<?php
namespace App\Requests\Settings;

use Anik\Form\FormRequest;

class MenuRequest extends FormRequest {
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'menus_name' => 'required',
            'menus_url' => 'required|unique:rsvp_menus,menus_url'
        ];
    }

    public function messages()
    {
        return [
            'menus_name.required' => 'Menu name is required !',
            'menus_url.required' => 'Menu URL is required !!',
            'menus_url.unique' => 'Menu URL is already added!!'
        ];
    }
}
