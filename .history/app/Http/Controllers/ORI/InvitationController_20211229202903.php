<?php

namespace App\Http\Controllers\ORI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

// Model
use App\Models\ORI\Invitations;
use App\Models\ORI\User;

class InvitationController extends Controller
{
    /**
        * @OA\Post(
        *     path="/ori/scanQRCode",
        *     operationId="/ori/invitation/scanQRCode",
        *     tags={"Invitations"},
        *     @OA\Parameter(
        *         name="client_id",
        *         in="query",
        *         description="Wedding client ID",
        *         required=true,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Parameter(
        *         name="qrCode",
        *         in="query",
        *         description="QR Code ID",
        *         required=true,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Response(
        *         response="200",
        *         description="Return",
        *         @OA\JsonContent(
        *              type="object",
        *              @OA\Property(property="status", type="boolean"),
        *              @OA\Property(property="data", type="array", @OA\Items()),
        *              @OA\Property(property="message", type="string")
        *         )
        *     ),
        *     @OA\Response(
        *         response="422",
        *         description="Error: Some validation not passed",
        *          @OA\JsonContent(
        *              type="array",
        *              @OA\Items()
        *         )
        *     ),
        *     @OA\Response(
        *         response="401",
        *         description="Error: username or password not right",
        *          @OA\JsonContent(
        *              type="object",
        *              @OA\Property(property="status", type="boolean"),
        *              @OA\Property(property="message", type="string"),
        *         )
        *     ),
        * )
    */
    public function checkQRCode(Request $req)
    {
        //validate incoming request
        $this->validate($req, [
            'client_id' => 'required|string',
            'qrCode' => 'required|string|exists:mysql_db_old.invitations,ivts_Uuid'
        ],[
           'qrCode.exists' => 'No QR code ID found !!'
        ]);

        try {
            $data = Invitations::select(
                '*',
                DB::raw("
                    CASE WHEN ivts_RsvpRespond = 101 THEN 'Archived'
                        WHEN ivts_RsvpRespond = 0 THEN 'Tidak Hadir'
                        WHEN ivts_RsvpRespond = 1 THEN ivts_RsvpGuest
                        WHEN ivts_RsvpRespond = 2 THEN 'Dibaca'
                        ELSE 'Baru'
                    END AS ivts_RsvpRespond
                ")
            )
                ->where('ivts_Uuid', $req->qrCode);

            if (!empty($req->client_id)) {
                $data->where('ivts_Client_Id', $req->client_id);
            }

            if (!empty($data->first())) {
                return response()->json([
                    'status' => true,
                    'message' => 'Guest Invitation found !!',
                    'data' => $data->first()
                ], 200);
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'Guest Invitation not found !!',
                    'data' => $data->first()
                ], 200);
            }
        } catch (\Throwable $th) {
            return response()->json( [
                'status' => false,
                'label' => $th->getMessage(),
                'data' => [
                    'entity' => 'invitation',
                    'action' => 'create',
                    'result' => $th->getMessage()
                ]
            ], 422);
        }
    }

    /**
        * @OA\Post(
        *     path="/ori/guestConfirmation",
        *     operationId="/ori/invitation/guestConfirmation",
        *     tags={"Invitations"},
        *     @OA\Parameter(
        *         name="client_id",
        *         in="query",
        *         description="Wedding client ID",
        *         required=true,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Parameter(
        *         name="qrCode",
        *         in="query",
        *         description="QR Code ID from client",
        *         required=true,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Parameter(
        *         name="guest_count",
        *         in="query",
        *         description="Guest count",
        *         required=true,
        *         @OA\Schema(type="integer")
        *     ),
        *     @OA\Parameter(
        *         name="souvenir_count",
        *         in="query",
        *         description="Souvenir get count",
        *         required=true,
        *         @OA\Schema(type="integer")
        *     ),
        *     @OA\Parameter(
        *         name="angpau_count",
        *         in="query",
        *         description="Angpau given count",
        *         required=true,
        *         @OA\Schema(type="integer")
        *     ),
        *     @OA\Parameter(
        *         name="operator",
        *         in="query",
        *         description="Operator logged in",
        *         required=false,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Response(
        *         response="200",
        *         description="Return",
        *         @OA\JsonContent(
        *              type="object",
        *              @OA\Property(property="status", type="boolean"),
        *              @OA\Property(property="data", type="array", @OA\Items()),
        *              @OA\Property(property="message", type="string")
        *         )
        *     ),
        *     @OA\Response(
        *         response="422",
        *         description="Error: Some validation not passed",
        *          @OA\JsonContent(
        *              type="array",
        *              @OA\Items()
        *         )
        *     ),
        *     @OA\Response(
        *         response="401",
        *         description="Error: username or password not right",
        *          @OA\JsonContent(
        *              type="object",
        *              @OA\Property(property="status", type="boolean"),
        *              @OA\Property(property="message", type="string"),
        *         )
        *     ),
        * )
    */
    public function guestConfirmation(Request $req)
    {
        //validate incoming request
        $this->validate($req, [
            'qrCode' => 'required|string',
            'client_id' => 'required|string',
            'guest_count' => 'required|integer',
            'souvenir_count' => 'required|integer',
            'angpau_count' => 'required|integer',
            'operator' => 'required|string',
        ],[
            'qrCode.exists' => 'No QR Code found in database !'
        ]);

        try {
            $cekInvitation = Invitations::select(
                'ivts_Name',
                'ivts_Address',
                DB::raw('SUM(ivts_GuestAtt) AS ivts_GuestAtt'),
                DB::raw('SUM(ivts_SouvenirAtt) AS ivts_SouvenirAtt'),
                DB::raw('SUM(ivts_AngpauAtt) AS ivts_AngpauAtt')
            )
            ->where('ivts_Uuid', $req->qrCode)
            ->where('ivts_RsvpStatus', '<>', NULL)
            ->groupBy(
                'ivts_Name',
                'ivts_Address'
            )
            ->first();

            if (!empty($cekInvitation)) {
                $this->guestRegister(new Request([
                    'client_id' => $req->client_id,
                    'qrCode' => $req->qrCode,
                    'guest_name' => $cekInvitation->ivts_Name,
                    'guest_addr' => $cekInvitation->ivts_Address,
                    'guest_count_pred' => $cekInvitation->ivts_GuestAtt,
                    'guest_count' => $req->guest_count,
                    'souvenir_count_pred' => $cekInvitation->ivts_SouvenirAtt,
                    'souvenir_count' => $req->souvenir_count,
                    'angpau_count_pred' => $req->ivts_AngpauAtt,
                    'angpau_count' => $req->angpau_count,
                    'operator' => $req->operator,
                    'status' => 'Iterasi'
                ]));
            } else {
                Invitations::where('ivts_Uuid', $req->qrCode)
                ->where('ivts_Client_Id', $req->client_id)
                ->update([
                    'ivts_GuestAtt' => $req->guest_count,
                    'ivts_GuestAttTime' => date('Y-m-d H:i:s'),
                    'ivts_GuestAttCounter' => $req->operator,
                    'ivts_SouvenirAtt' => $req->souvenir_count,
                    'ivts_SouvenirAttTime' => date('Y-m-d H:i:s'),
                    'ivts_SouvenirAttCounter' => $req->operator,
                    'ivts_AngpauAtt' => $req->angpau_count,
                    'ivts_AngpauAttTime' => date('Y-m-d H:i:s'),
                    'ivts_AngpauAttCounter' => $req->operator,
                    'ivts_RsvpStatus' => 'RSVP'
                ]);
            }

            return response()->json( [
                'status' => true,
                'label' => 'Invitation updated !',
                'data' => Invitations::where('ivts_Uuid', $req->qrCode)->get()->toArray()
            ], 200);
        } catch (\Throwable $th) {
            return response()->json( [
                'status' => false,
                'label' => $th->getMessage(),
                'data' => [
                    'entity' => 'invitation',
                    'action' => 'create',
                    'result' => $th->getMessage()
                ]
            ], 422);
        }
    }

    /**
        * @OA\Post(
        *     path="/ori/guestRegister",
        *     operationId="/ori/invitation/guestRegister",
        *     tags={"Invitations"},
        *     @OA\Parameter(
        *         name="client_id",
        *         in="query",
        *         description="Wedding client ID",
        *         required=true,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Parameter(
        *         name="guest_name",
        *         in="query",
        *         description="Guest name",
        *         required=true,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Parameter(
        *         name="guest_addr",
        *         in="query",
        *         description="Guest Address / Instance",
        *         required=true,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Parameter(
        *         name="guest_count",
        *         in="query",
        *         description="Guest count",
        *         required=true,
        *         @OA\Schema(type="integer")
        *     ),
        *     @OA\Parameter(
        *         name="souvenir_count",
        *         in="query",
        *         description="Souvenir get count",
        *         required=true,
        *         @OA\Schema(type="integer")
        *     ),
        *     @OA\Parameter(
        *         name="angpau_count",
        *         in="query",
        *         description="Angpau given count",
        *         required=true,
        *         @OA\Schema(type="integer")
        *     ),
        *     @OA\Parameter(
        *         name="operator",
        *         in="query",
        *         description="Operator logged in",
        *         required=false,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Response(
        *         response="200",
        *         description="Return",
        *         @OA\JsonContent(
        *              type="object",
        *              @OA\Property(property="status", type="boolean"),
        *              @OA\Property(property="data", type="array", @OA\Items()),
        *              @OA\Property(property="message", type="string")
        *         )
        *     ),
        *     @OA\Response(
        *         response="422",
        *         description="Error: there is an error on the server / some validation not work",
        *          @OA\JsonContent(
        *              type="object",
        *              @OA\Property(property="status", type="boolean"),
        *              @OA\Property(property="message", type="string"),
        *         )
        *     ),
        * )
    */
    public function guestRegister(Request $req)
    {
        //validate incoming request
        $this->validate($req, [
            'client_id' => 'required|string',
            'guest_name' => 'required|string',
            'guest_addr' => 'required|string',
            'guest_count' => 'required|integer',
            'souvenir_count' => 'required|integer',
            'angpau_count' => 'required|integer',
            'operator' => 'required|string'
        ]);

        try {
            $id = $this->checkID('');
            $data = Invitations::create([
                'ivts_Guest' => !$req->has('guest_count_pred') || empty($req->guest_count_pred) ? $req->guest_count : $req->guest_count_pred,
                'ivts_Client_Id' => $req->client_id,
                'ivts_Uuid' => !$req->has('qrCode') || empty($req->qrCode) ? $id : $req->qrCode,
                'ivts_Name' => $req->guest_name,
                'ivts_Address' => $req->guest_addr,
                'ivts_GuestAtt' => $req->guest_count,
                'ivts_GuestAttTime' => date('Y-m-d H:i:s'),
                'ivts_GuestAttCounter' => $req->operator,
                'ivts_Souvenir' => !$req->has('souvenir_count_pred') || empty($req->souvenir_count_pred) ? $req->souvenir_count : $req->souvenir_count_pred,
                'ivts_SouvenirAtt' => $req->souvenir_count,
                'ivts_SouvenirAttTime' => date('Y-m-d H:i:s'),
                'ivts_SouvenirAttCounter' => $req->operator,
                'ivts_Angpau' => !$req->has('angpau_count_pred') || empty($req->angpau_count_pred) ? $req->angpau_count : $req->angpau_count_pred,
                'ivts_AngpauAtt' => $req->angpau_count,
                'ivts_AngpauAttTime' => date('Y-m-d H:i:s'),
                'ivts_AngpauAttCounter' => $req->operator,
                'ivts_RsvpStatus' => $req->has('status') && empty($req->status) ? 'Input Manual' : $req->status
            ]);

            return response()->json( [
                'status' => true,
                'label' => 'Guest added !',
                'data' => $data
            ], 200);
        } catch (\Throwable $th) {
            return response()->json( [
                'status' => false,
                'label' => $th->getMessage(),
                'data' => [
                    'entity' => 'invitation',
                    'action' => 'create',
                    'result' => $th->getMessage()
                ]
            ], 422);
        }
    }

    public function checkID($id)
    {
        if (empty($id)) {
            $ids = $this->generateRandomString(8).'-'.$this->generateRandomString(4).'-'.$this->generateRandomString(4).'-'.$this->generateRandomString(4).'-'.$this->generateRandomString(12);

            $cekData = Invitations::where('ivts_Uuid', $ids)->first();

            if (empty($cekData)) {
                return $ids;
            } else {
                return $this->checkID('');
            }
        } else {
            $cekData = Invitations::where('ivts_Uuid', $id)->first();
            if (empty($cekData)) {
                return $id;
            } else {
                return $this->checkID('');
            }
        }
    }

    public function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
        * @OA\Post(
        *     path="/ori/searchGuest",
        *     operationId="/ori/invitation/searchGuest",
        *     tags={"Invitations"},
        *     @OA\Parameter(
        *         name="client_id",
        *         in="query",
        *         description="Wedding client ID",
        *         required=true,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Parameter(
        *         name="query_search",
        *         in="query",
        *         description="Search Invitation by name",
        *         required=true,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Response(
        *         response="200",
        *         description="Return",
        *         @OA\JsonContent(
        *              type="object",
        *              @OA\Property(property="status", type="boolean"),
        *              @OA\Property(property="data", type="array", @OA\Items()),
        *              @OA\Property(property="message", type="string")
        *         )
        *     ),
        *     @OA\Response(
        *         response="422",
        *         description="Error: there is an error on the server / backend",
        *          @OA\JsonContent(
        *              type="object",
        *              @OA\Property(property="status", type="boolean"),
        *              @OA\Property(property="message", type="string"),
        *         )
        *     ),
        * )
    */
    public function searchGuest(Request $req)
    {
        //validate incoming request
        $this->validate($req, [
            'client_id' => 'required|string',
            'query_search' => 'required|string'
        ]);

        try {
            $data = Invitations::select(
                'ivts_Name',
                'ivts_Address',
                DB::raw('MAX(ivts_Guest) AS ivts_Guest'),
                DB::raw('SUM(ivts_GuestAtt) AS ivts_GuestAtt'),
                DB::raw('MAX(ivts_GuestAttTime) AS ivts_GuestAttTime'),
                DB::raw('MAX(ivts_Souvenir) AS ivts_Souvenir'),
                DB::raw('SUM(ivts_SouvenirAtt) AS ivts_SouvenirAtt'),
                DB::raw('MAX(ivts_SouvenirAttTime) AS ivts_SouvenirAttTime'),
                DB::raw('MAX(ivts_Angpau) AS ivts_Angpau'),
                DB::raw('SUM(ivts_AngpauAtt) AS ivts_AngpauAtt'),
                DB::raw('MAX(ivts_AngpauAttTime) AS ivts_AngpauAttTime'),
                DB::raw('MAX(ivts_AngpauAttCounter) AS ivts_AngpauAttCounter'),
                DB::raw('MAX(ivts_Category) AS ivts_Category'),
            )
                ->where('ivts_Client_Id', $req->client_id)
                ->where('ivts_Name', 'like', '%'.$req->query_search.'%')
                // ->where('ivts_RsvpStatus', '<>', 'iterasi')
                ->get()
                ->toArray();

            if (count($data) > 0) {
                return response()->json( [
                    'status' => true,
                    'label' => 'Search found !',
                    'data' => $data
                ], 200);
            } else {
                return response()->json( [
                    'status' => false,
                    'label' => 'Search not found !',
                    'data' => $data
                ], 422);
            }
        } catch (\Throwable $th) {
            return response()->json( [
                'status' => false,
                'label' => $th->getMessage(),
                'data' => [
                    'entity' => 'invitation',
                    'action' => 'create',
                    'result' => $th->getMessage()
                ]
            ], 422);
        }
    }
}
