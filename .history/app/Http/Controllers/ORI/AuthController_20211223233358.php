<?php

namespace App\Http\Controllers\ORI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

// Model
use App\Models\ORI\User;

class AuthController extends Controller
{
    public function login(Request $req)
    {
         //validate incoming request
         $this->validate($req, [
            'username' => 'required|string',
            'password' => 'required|string',
        ]);

        $credentials = $req->only(['username', 'password']);

        if (! $token = User::attempt($credentials)) {
            return response()->json([
                'status' => false,
                'message' => 'Unauthorized'
            ], 401);
        }

        $dataUser = User::where('username', $req->username)->first();
        return array_merge(
            [
                'status' => true,
                'message' => 'Authenticated success',
                'data' => $dataUser->userDetail ? $dataUser->userDetail->with(['category.categoryMaster' => function ($r) {
                    $r->select('cat_name', 'cat_desc');
                }])->first() : $dataUser->userDetail
            ],
            $this->respondWithToken($token)->original
        );
    }
}
