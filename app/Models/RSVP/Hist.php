<?php

namespace App\Models\RSVP;

use Illuminate\Database\Eloquent\Model;

class Hist extends Model {
    protected $table = 'rsvp_hist';
    protected $fillable = [
        'username',
        'hist_user_fam_count',
        'hist_user_souvenir_count',
        'hist_user_angpau_count',
        'desc',
        'username_registered',
    ];
}
