<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRsvpMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rsvp_menus', function (Blueprint $table) {
            $table->id();
            $table->string('menus_id', 10);
            $table->string('menus_name', 100);
            $table->string('menus_url', 100)->nullable();
            $table->string('menus_parent', 10)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rsvp_menus');
    }
}
