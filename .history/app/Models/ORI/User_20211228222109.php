<?php

namespace App\Models\ORI;

use Illuminate\Database\Eloquent\Model;

class User extends Model {
    protected $connection = 'mysql_db_old';
    protected $table = 'administrator';
    protected $hidden = [
        'admnPassword',
    ];
    protected $fillable = [
        'admnId',
        'admnUsername',
        'admnPassword',
        'admnFirstname',
        'admnLastName',
        'admnEmail',
        'admnPhone',
        'admnLastLogin',
        'admnLevel',
        'admnJti',
        'admnCliId',
        'admnUsrId',
        'admnCliMode',
        'admnAppCamFacing',
        'admnCreated',
        'admnUpdated',
        'admnDeleted',
        'admnIsDeleted',
    ];

    public function clients()
    {
        return $this->hasOne('App\Models\ORI\Clients', 'client_admnCliId', 'admnCliId');
    }
}
