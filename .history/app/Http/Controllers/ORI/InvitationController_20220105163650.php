<?php

namespace App\Http\Controllers\ORI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

// Model
use App\Models\ORI\Invitations;
use App\Models\ORI\User;

use Illuminate\Log\Logger;

class InvitationController extends Controller
{
    /**
        * @OA\Post(
        *     path="/ori/scanQRCode",
        *     operationId="/ori/invitation/scanQRCode",
        *     tags={"Invitations"},
        *     @OA\Parameter(
        *         name="client_id",
        *         in="query",
        *         description="Wedding client ID",
        *         required=true,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Parameter(
        *         name="qrCode",
        *         in="query",
        *         description="QR Code ID",
        *         required=true,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Response(
        *         response="200",
        *         description="Return",
        *         @OA\JsonContent(
        *              type="object",
        *              @OA\Property(property="status", type="boolean"),
        *              @OA\Property(property="data", type="array", @OA\Items()),
        *              @OA\Property(property="message", type="string")
        *         )
        *     ),
        *     @OA\Response(
        *         response="422",
        *         description="Error: Some validation not passed",
        *          @OA\JsonContent(
        *              type="array",
        *              @OA\Items()
        *         )
        *     ),
        *     @OA\Response(
        *         response="401",
        *         description="Error: username or password not right",
        *          @OA\JsonContent(
        *              type="object",
        *              @OA\Property(property="status", type="boolean"),
        *              @OA\Property(property="message", type="string"),
        *         )
        *     ),
        * )
    */
    public function checkQRCode(Request $req)
    {
        //validate incoming request
        $this->validate($req, [
            'client_id' => 'required|string',
            'qrCode' => 'required|string|exists:mysql_db_old.invitations,ivts_Uuid'
        ],[
           'qrCode.exists' => 'No QR code ID found !!'
        ]);

        try {
            $data = Invitations::select(
                'ivts_Client_Id',
                'ivts_Uuid',
                'ivts_Name',
                'ivts_Address',
                DB::raw('COUNT(*) AS ivts_TotalAtt'),
                DB::raw('MAX(ivts_NoHp) AS ivts_NoHp'),
                DB::raw('MAX(ivts_Guest) AS ivts_Guest'),
                DB::raw('COALESCE(SUM(ivts_GuestAtt),0) AS ivts_GuestAtt'),
                DB::raw('MAX(ivts_GuestAttTime) AS ivts_GuestAttTime'),
                DB::raw('MAX(ivts_GuestAttCounter) AS ivts_GuestAttCounter'),
                DB::raw('MAX(ivts_Souvenir) AS ivts_Souvenir'),
                DB::raw('COALESCE(SUM(ivts_SouvenirAtt),0) AS ivts_SouvenirAtt'),
                DB::raw('MAX(ivts_SouvenirAttTime) AS ivts_SouvenirAttTime'),
                DB::raw('MAX(ivts_SouvenirAttCounter) AS ivts_SouvenirAttCounter'),
                DB::raw('MAX(ivts_Angpau) AS ivts_Angpau'),
                DB::raw('COALESCE(SUM(ivts_AngpauAtt),0) AS ivts_AngpauAtt'),
                DB::raw('MAX(ivts_AngpauAttTime) AS ivts_AngpauAttTime'),
                DB::raw('MAX(ivts_AngpauAttCounter) AS ivts_AngpauAttCounter'),
                DB::raw('MAX(ivts_Category) AS ivts_Category'),
                DB::raw('MAX(ivts_Group) AS ivts_Group'),
                DB::raw('MAX(ivts_GroupFam) AS ivts_GroupFam'),
                DB::raw('MAX(ivts_GroupSub) AS ivts_GroupSub'),
                DB::raw('MAX(ivts_Seat) AS ivts_Seat'),
                DB::raw('MAX(ivts_RsvpStatus) AS ivts_RsvpStatus'),
                DB::raw("
                    CASE WHEN ivts_RsvpRespond = 0 THEN 'Tidak Hadir'
                        WHEN ivts_RsvpRespond = 1 THEN CONCAT('Hadir', '(', MAX(ivts_RsvpGuest), ')')
                        ELSE 'Belum Konfirmasi'
                    END AS ivts_RsvpRespond
                ")
            )
                ->where('ivts_Uuid', $req->qrCode);

            if (!empty($req->client_id)) {
                $data->where('ivts_Client_Id', $req->client_id);
            }

            $data->groupBy(
                'ivts_Client_Id',
                'ivts_Uuid',
                'ivts_Name',
                'ivts_Address'
            );

            if (!empty($data->first())) {
                return response()->json([
                    'status' => true,
                    'message' => 'Guest Invitation found !!',
                    'data' => $data->first()
                ], 200);
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'Guest Invitation not found !!',
                    'data' => $data->first()
                ], 200);
            }
        } catch (\Throwable $th) {
            return response()->json( [
                'status' => false,
                'label' => $th->getMessage(),
                'data' => [
                    'entity' => 'checkQRCode',
                    'action' => 'create',
                    'result' => $th->getMessage()
                ]
            ], 422);
        }
    }

    /**
        * @OA\Post(
        *     path="/ori/guestConfirmation",
        *     operationId="/ori/invitation/guestConfirmation",
        *     tags={"Invitations"},
        *     @OA\Parameter(
        *         name="client_id",
        *         in="query",
        *         description="Wedding client ID",
        *         required=true,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Parameter(
        *         name="qrCode",
        *         in="query",
        *         description="QR Code ID from client",
        *         required=true,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Parameter(
        *         name="guest_count",
        *         in="query",
        *         description="Guest count",
        *         required=true,
        *         @OA\Schema(type="integer")
        *     ),
        *     @OA\Parameter(
        *         name="souvenir_count",
        *         in="query",
        *         description="Souvenir get count",
        *         required=true,
        *         @OA\Schema(type="integer")
        *     ),
        *     @OA\Parameter(
        *         name="angpau_count",
        *         in="query",
        *         description="Angpau given count",
        *         required=true,
        *         @OA\Schema(type="integer")
        *     ),
        *     @OA\Parameter(
        *         name="operator",
        *         in="query",
        *         description="Operator logged in",
        *         required=false,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Response(
        *         response="200",
        *         description="Return",
        *         @OA\JsonContent(
        *              type="object",
        *              @OA\Property(property="status", type="boolean"),
        *              @OA\Property(property="data", type="array", @OA\Items()),
        *              @OA\Property(property="message", type="string")
        *         )
        *     ),
        *     @OA\Response(
        *         response="422",
        *         description="Error: Some validation not passed",
        *          @OA\JsonContent(
        *              type="array",
        *              @OA\Items()
        *         )
        *     ),
        *     @OA\Response(
        *         response="401",
        *         description="Error: username or password not right",
        *          @OA\JsonContent(
        *              type="object",
        *              @OA\Property(property="status", type="boolean"),
        *              @OA\Property(property="message", type="string"),
        *         )
        *     ),
        * )
    */
    public function guestConfirmation(Request $req)
    {
        date_default_timezone_set('Asia/Jakarta');
        //validate incoming request
        $this->validate($req, [
            'qrCode' => 'required|string',
            'client_id' => 'required|string',
            // 'guest_count' => 'required|integer',
            // 'souvenir_count' => 'required|integer',
            // 'angpau_count' => 'required|integer',
            'operator' => 'required|string',
        ],[
            'qrCode.exists' => 'No QR Code found in database !'
        ]);

        try {
            $cekInvitation = Invitations::select(
                'ivts_Name',
                'ivts_Address',
                'ivts_NoHp',
                'ivts_Seat',
                'ivts_Group',
                'ivts_GroupFam',
                'ivts_GroupSub',
                DB::raw('COALESCE(SUM(ivts_GuestAtt),0) AS ivts_GuestAtt'),
                DB::raw('COALESCE(SUM(ivts_SouvenirAtt),0) AS ivts_SouvenirAtt'),
                DB::raw('COALESCE(SUM(ivts_AngpauAtt),0) AS ivts_AngpauAtt')
            )
            ->where('ivts_Uuid', $req->qrCode)
            // ->where('ivts_GuestAtt', '>', 0)
            ->groupBy(
                'ivts_Name',
                'ivts_Address',
                'ivts_NoHp',
                'ivts_Seat',
                'ivts_Group',
                'ivts_GroupFam',
                'ivts_GroupSub'
            )
            ->first();

            // return $cekInvitation;

            if (!empty($cekInvitation)) {
                $this->guestRegister(new Request([
                    'client_id' => $req->client_id,
                    'qrCode' => $req->qrCode,
                    'guest_name' => $cekInvitation->ivts_Name,
                    'guest_addr' => $cekInvitation->ivts_Address,
                    'guest_count_pred' => (int)$cekInvitation->ivts_Guest,
                    'guest_count' => $req->guest_count,
                    'souvenir_count_pred' => (int)$cekInvitation->ivts_Souvenir,
                    'souvenir_count' => $req->souvenir_count,
                    'angpau_count_pred' => (int)$cekInvitation->ivts_Angpau,
                    'angpau_count' => $req->angpau_count,
                    'operator' => $req->operator,
                    'no_hp' => $cekInvitation->ivts_NoHp,
                    'seat' => $cekInvitation->ivts_Seat,
                    'group' => $cekInvitation->ivts_Group,
                    'group_fam' => $cekInvitation->ivts_GroupFam,
                    'group_sub' => $cekInvitation->ivts_GroupSub,
                    'status' => 'Iterasi'
                ]));
            } else {
                Invitations::where('ivts_Uuid', $req->qrCode)
                ->where('ivts_Client_Id', $req->client_id)
                ->update([
                    'ivts_GuestAtt' => $req->guest_count,
                    'ivts_GuestAttTime' => date('Y-m-d H:i:s'),
                    'ivts_GuestAttCounter' => $req->operator,
                    'ivts_SouvenirAtt' => $req->souvenir_count,
                    'ivts_SouvenirAttTime' => date('Y-m-d H:i:s'),
                    'ivts_SouvenirAttCounter' => $req->operator,
                    'ivts_AngpauAtt' => $req->angpau_count,
                    'ivts_AngpauAttTime' => date('Y-m-d H:i:s'),
                    'ivts_AngpauAttCounter' => $req->operator
                    // 'ivts_RsvpStatus' => 'RSVP'
                ]);
            }

            return response()->json( [
                'status' => true,
                'label' => 'Invitation updated !',
                'data' => Invitations::where('ivts_Uuid', $req->qrCode)->get()->toArray()
            ], 200);
        } catch (\Throwable $th) {
            return response()->json( [
                'status' => false,
                'label' => $th->getMessage(),
                'data' => [
                    'entity' => 'guestConfirmation',
                    'action' => 'create',
                    'result' => $th
                ]
            ], 422);
        }
    }

    /**
        * @OA\Post(
        *     path="/ori/guestRegister",
        *     operationId="/ori/invitation/guestRegister",
        *     tags={"Invitations"},
        *     @OA\Parameter(
        *         name="client_id",
        *         in="query",
        *         description="Wedding client ID",
        *         required=true,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Parameter(
        *         name="guest_name",
        *         in="query",
        *         description="Guest name",
        *         required=true,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Parameter(
        *         name="guest_addr",
        *         in="query",
        *         description="Guest Address / Instance",
        *         required=true,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Parameter(
        *         name="guest_count",
        *         in="query",
        *         description="Guest count",
        *         required=true,
        *         @OA\Schema(type="integer")
        *     ),
        *     @OA\Parameter(
        *         name="souvenir_count",
        *         in="query",
        *         description="Souvenir get count",
        *         required=true,
        *         @OA\Schema(type="integer")
        *     ),
        *     @OA\Parameter(
        *         name="angpau_count",
        *         in="query",
        *         description="Angpau given count",
        *         required=true,
        *         @OA\Schema(type="integer")
        *     ),
        *     @OA\Parameter(
        *         name="operator",
        *         in="query",
        *         description="Operator logged in",
        *         required=false,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Response(
        *         response="200",
        *         description="Return",
        *         @OA\JsonContent(
        *              type="object",
        *              @OA\Property(property="status", type="boolean"),
        *              @OA\Property(property="data", type="array", @OA\Items()),
        *              @OA\Property(property="message", type="string")
        *         )
        *     ),
        *     @OA\Response(
        *         response="422",
        *         description="Error: there is an error on the server / some validation not work",
        *          @OA\JsonContent(
        *              type="object",
        *              @OA\Property(property="status", type="boolean"),
        *              @OA\Property(property="message", type="string"),
        *         )
        *     ),
        * )
    */
    public function guestRegister(Request $req)
    {
        date_default_timezone_set('Asia/Jakarta');
        //validate incoming request
        $this->validate($req, [
            'client_id' => 'required|string',
            'guest_name' => 'required|string',
            // 'guest_addr' => 'required|string',
            // 'guest_count' => 'required|integer',
            // 'souvenir_count' => 'required|integer',
            // 'angpau_count' => 'required|integer',
            'operator' => 'required|string'
        ]);

        try {
            // return $req;
            $id = $this->checkID('');
            $data = Invitations::create([
                'ivts_Guest' => !$req->has('guest_count_pred') || empty($req->guest_count_pred) ? $req->guest_count : $req->guest_count_pred,
                'ivts_Client_Id' => $req->client_id,
                'ivts_Uuid' => !$req->has('qrCode') || empty($req->qrCode) ? $id : $req->qrCode,
                'ivts_Name' => $req->guest_name,
                'ivts_Address' => $req->guest_addr,
                'ivts_NoHp' => $req->has('no_hp') && !empty($req->no_hp) ? $req->no_hp : '',
                'ivts_Seat' => $req->has('seat') && !empty($req->seat) ? $req->seat : '',
                'ivts_Group' => $req->has('group') && !empty($req->group) ? $req->group : '',
                'ivts_GroupFam' => $req->has('group_fam') && !empty($req->group_fam) ? $req->group_fam : '',
                'ivts_GroupSub' => $req->has('group_sub') && !empty($req->group_sub) ? $req->group_sub : '',
                'ivts_GuestAtt' => $req->guest_count,
                'ivts_GuestAttTime' => date('Y-m-d H:i:s'),
                'ivts_GuestAttCounter' => $req->operator,
                'ivts_Souvenir' => !$req->has('souvenir_count_pred') || empty($req->souvenir_count_pred) ? $req->souvenir_count : $req->souvenir_count_pred,
                'ivts_SouvenirAtt' => $req->souvenir_count,
                'ivts_SouvenirAttTime' => date('Y-m-d H:i:s'),
                'ivts_SouvenirAttCounter' => $req->operator,
                'ivts_Angpau' => !$req->has('angpau_count_pred') || empty($req->angpau_count_pred) ? $req->angpau_count : $req->angpau_count_pred,
                'ivts_AngpauAtt' => $req->angpau_count,
                'ivts_AngpauAttTime' => date('Y-m-d H:i:s'),
                'ivts_AngpauAttCounter' => $req->operator,
                'ivts_RsvpStatus' => !$req->has('status') || empty($req->status) ? 'Input Manual' : $req->status,
                'ivts_RsvpRespond' => !$req->has('respond') || empty($req->respond) ? 1 : $req->respond,
            ]);

            return response()->json( [
                'status' => true,
                'label' => 'Guest added !',
                'data' => $data
            ], 200);
        } catch (\Throwable $th) {
            return response()->json( [
                'status' => false,
                'label' => $th->getMessage(),
                'data' => [
                    'entity' => 'guestRegister',
                    'action' => 'create',
                    'result' => $th->getMessage()
                ]
            ], 422);
        }
    }

    public function checkID($id)
    {
        if (empty($id)) {
            $ids = $this->generateRandomString(8).'-'.$this->generateRandomString(4).'-'.$this->generateRandomString(4).'-'.$this->generateRandomString(4).'-'.$this->generateRandomString(12);

            $cekData = Invitations::where('ivts_Uuid', $ids)->first();

            if (empty($cekData)) {
                return $ids;
            } else {
                return $this->checkID('');
            }
        } else {
            $cekData = Invitations::where('ivts_Uuid', $id)->first();
            if (empty($cekData)) {
                return $id;
            } else {
                return $this->checkID('');
            }
        }
    }

    public function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
        * @OA\Post(
        *     path="/ori/searchGuest",
        *     operationId="/ori/invitation/searchGuest",
        *     tags={"Invitations"},
        *     @OA\Parameter(
        *         name="client_id",
        *         in="query",
        *         description="Wedding client ID",
        *         required=true,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Parameter(
        *         name="query_search",
        *         in="query",
        *         description="Search Invitation by name",
        *         required=true,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Response(
        *         response="200",
        *         description="Return",
        *         @OA\JsonContent(
        *              type="object",
        *              @OA\Property(property="status", type="boolean"),
        *              @OA\Property(property="data", type="array", @OA\Items()),
        *              @OA\Property(property="message", type="string")
        *         )
        *     ),
        *     @OA\Response(
        *         response="422",
        *         description="Error: there is an error on the server / backend",
        *          @OA\JsonContent(
        *              type="object",
        *              @OA\Property(property="status", type="boolean"),
        *              @OA\Property(property="message", type="string"),
        *         )
        *     ),
        * )
    */
    public function searchGuest(Request $req)
    {
        //validate incoming request
        $this->validate($req, [
            'client_id' => 'required|string',
            'query_search' => 'required|string'
        ]);

        try {
            $data = DB::connection('mysql_db_old')->table('invitations as inv2')->select(
                'ivts_Client_Id',
                'ivts_Uuid',
                'ivts_Name',
                'ivts_Address',
                DB::raw("COUNT(*) + (
                    SELECT COUNT(*)
                    FROM invitations inv
                    WHERE inv.ivts_RsvpStatus = 'Iterasi'
                    and inv.ivts_Client_Id = inv2.ivts_Client_Id
                    and inv.ivts_Uuid = inv2.ivts_Uuid
                ) AS ivts_TotalAtt"),
                DB::raw('MAX(ivts_NoHp) AS ivts_NoHp'),
                DB::raw('MAX(ivts_Guest) AS ivts_Guest'),
                DB::raw("COALESCE(SUM(ivts_GuestAtt) + (
                    SELECT COALESCE(SUM(ivts_GuestAtt),0)
                    FROM invitations inv
                    WHERE inv.ivts_RsvpStatus = 'Iterasi'
                    and inv.ivts_Client_Id = inv2.ivts_Client_Id
                    and inv.ivts_Uuid = inv2.ivts_Uuid
                ),0) AS ivts_GuestAtt"),
                DB::raw('MAX(ivts_GuestAttTime) AS ivts_GuestAttTime'),
                DB::raw('MAX(ivts_GuestAttCounter) AS ivts_GuestAttCounter'),
                DB::raw('MAX(ivts_Souvenir) AS ivts_Souvenir'),
                DB::raw("COALESCE(SUM(ivts_SouvenirAtt) + (
                    SELECT COALESCE(SUM(ivts_SouvenirAtt),0)
                    FROM invitations inv
                    WHERE inv.ivts_RsvpStatus = 'Iterasi'
                    and inv.ivts_Client_Id = inv2.ivts_Client_Id
                    and inv.ivts_Uuid = inv2.ivts_Uuid
                ),0) AS ivts_SouvenirAtt"),
                DB::raw('MAX(ivts_SouvenirAttTime) AS ivts_SouvenirAttTime'),
                DB::raw('MAX(ivts_SouvenirAttCounter) AS ivts_SouvenirAttCounter'),
                DB::raw('MAX(ivts_Angpau) AS ivts_Angpau'),
                DB::raw("COALESCE(SUM(ivts_AngpauAtt) + (
                    SELECT COALESCE(SUM(ivts_AngpauAtt),0)
                    FROM invitations inv
                    WHERE inv.ivts_RsvpStatus = 'Iterasi'
                    and inv.ivts_Client_Id = inv2.ivts_Client_Id
                    and inv.ivts_Uuid = inv2.ivts_Uuid
                ),0) AS ivts_AngpauAtt"),
                DB::raw('MAX(ivts_AngpauAttTime) AS ivts_AngpauAttTime'),
                DB::raw('MAX(ivts_AngpauAttCounter) AS ivts_AngpauAttCounter'),
                DB::raw('MAX(ivts_Category) AS ivts_Category'),
                DB::raw('MAX(ivts_Group) AS ivts_Group'),
                DB::raw('MAX(ivts_GroupFam) AS ivts_GroupFam'),
                DB::raw('MAX(ivts_GroupSub) AS ivts_GroupSub'),
                DB::raw('MAX(ivts_Seat) AS ivts_Seat'),
                DB::raw('MAX(ivts_RsvpStatus) AS ivts_RsvpStatus'),
                DB::raw("
                    CASE WHEN MAX(ivts_RsvpRespond) = 0 THEN 'Tidak Hadir'
                        WHEN MAX(ivts_RsvpRespond) = 1 THEN CONCAT('Hadir', '(', MAX(ivts_RsvpGuest), ')')
                        ELSE 'Belum Konfirmasi'
                    END AS ivts_RsvpRespond
                ")
            )
                ->where('ivts_Client_Id', $req->client_id)
                ->where('ivts_Name', 'like', '%'.$req->query_search.'%')
                ->whereIn('ivts_RsvpStatus', ['RSVP', 'Undangan'])
                ->groupBy(
                    'ivts_Client_Id',
                    'ivts_Uuid',
                    'ivts_Name',
                    'ivts_Address'
                )
                ->get();

            // return $data;
            if (count($data) > 0) {
                return response()->json( [
                    'status' => true,
                    'label' => 'Search found !',
                    'data' => $data
                ], 200);
            } else {
                return response()->json( [
                    'status' => false,
                    'label' => 'Search not found !',
                    'data' => $data
                ], 422);
            }
        } catch (\Throwable $th) {
            return response()->json( [
                'status' => false,
                'label' => $th->getMessage(),
                'data' => [
                    'entity' => 'searchGuest',
                    'action' => 'create',
                    'result' => $th->getMessage()
                ]
            ], 422);
        }
    }

    /**
        * @OA\Post(
        *     path="/ori/dashboardCard",
        *     operationId="/ori/invitation/dashboardCard",
        *     tags={"Invitations"},
        *     @OA\Parameter(
        *         name="client_id",
        *         in="query",
        *         description="Wedding client ID",
        *         required=true,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Response(
        *         response="200",
        *         description="Return",
        *         @OA\JsonContent(
        *              type="object",
        *              @OA\Property(property="status", type="boolean"),
        *              @OA\Property(property="data", type="array", @OA\Items()),
        *              @OA\Property(property="message", type="string")
        *         )
        *     ),
        *     @OA\Response(
        *         response="422",
        *         description="Error: there is an error on the server / backend",
        *          @OA\JsonContent(
        *              type="object",
        *              @OA\Property(property="status", type="boolean"),
        *              @OA\Property(property="message", type="string"),
        *         )
        *     ),
        * )
    */
    public function dashboardCard(Request $req)
    {
        //validate incoming request
        $this->validate($req, [
            'client_id' => 'required|string'
        ]);

        try {
            $dataRSVP = DB::connection('mysql_db_old')->table('invitations as inv2')->where('ivts_Client_Id', $req->client_id)->whereIn('ivts_RsvpStatus', ['RSVP', 'Undangan']);
            $dataManual = DB::connection('mysql_db_old')->table('invitations as inv2')->where('ivts_Client_Id', $req->client_id)->where('ivts_RsvpStatus', 'Input Manual');
            $dataTotal = DB::connection('mysql_db_old')->table('invitations as inv2')->where('ivts_Client_Id', $req->client_id)->whereIn('ivts_RsvpStatus', ['RSVP', 'Undangan', 'Input Manual']);

            if ($req->has('qrCode') && !empty($req->qrCode)) {
                $dataRSVP->where('ivts_Uuid', $req->qrCode);
                $dataManual->where('ivts_Uuid', $req->qrCode);
                $dataTotal->where('ivts_Uuid', $req->qrCode);
            }

            if ($req->has('column') && !empty($req->column)) {
                if ($req->has('filter') && !empty($req->filter)) {
                    $dataRSVP->where($req->column, 'like', '%'.$req->filter.'%');
                    $dataManual->where($req->column, 'like', '%'.$req->filter.'%');
                    $dataTotal->where($req->column, 'like', '%'.$req->filter.'%');
                }
            } else {
                if ($req->has('filter') && !empty($req->filter)) {
                    $dataRSVP->where('ivts_Name', 'like', '%'.$req->filter.'%');
                    $dataManual->where('ivts_Name', 'like', '%'.$req->filter.'%');
                    $dataTotal->where('ivts_Name', 'like', '%'.$req->filter.'%');
                }
            }

            $hasil = [
                'RSVP' => [
                    'TOTAL' => (clone $dataRSVP)->count(),
                    'HADIR' => [
                        'ORANG' => (int)(clone $dataRSVP)->where('ivts_RsvpRespond', 1)->select(DB::raw('COALESCE(SUM(ivts_RsvpGuest),0) as tot'))->first()->tot,
                        'UNDANGAN' => (clone $dataRSVP)->where('ivts_RsvpRespond', 1)->count()
                    ],
                    'TIDAK_HADIR' => [
                        'CONFIRM_RSVP' => (clone $dataRSVP)->select(DB::raw('COALESCE(SUM(ivts_GuestAtt),0) as tot'))->where('ivts_RsvpRespond', 0)->where('ivts_GuestAtt', '>', 0)->count(),
                        'TOTAL' => (clone $dataRSVP)->where('ivts_RsvpRespond', 0)->count()
                    ],
                    'UNCONFIRM' => [
                        'CONFIRM_RSVP' => (clone $dataRSVP)->select(DB::raw('COALESCE(SUM(ivts_GuestAtt),0) as tot'))->where(function($w) {
                            $w->whereNotIn('ivts_RsvpRespond', [1, 0]);
                            $w->orWhere('ivts_RsvpRespond', NULL);
                        })->where('ivts_GuestAtt', '>', 0)->count(),
                        'TOTAL' => (clone $dataRSVP)->where(function($w) {
                            $w->whereNotIn('ivts_RsvpRespond', [1, 0]);
                            $w->orWhere('ivts_RsvpRespond', NULL);
                        })->count()
                    ]
                ],
                'UNDANGAN' => [
                    'HADIR' => [
                        'CONFIRM_RSVP' => (clone $dataRSVP)->select(DB::raw('COALESCE(SUM(ivts_GuestAtt),0) as tot'))->where('ivts_GuestAtt', '>', 0)->count(),
                        'PRED_RSVP' => (int)(clone $dataRSVP)->count()
                    ],
                    'GUEST' => [
                        'CONFIRM_RSVP' => (int)(clone $dataRSVP)->select(
                                $req->has('filter') && !empty($req->filter)
                                ? DB::raw("
                                    COALESCE(SUM(ivts_GuestAtt),0) +
                                    (
                                        SELECT COALESCE(SUM(ivts_GuestAtt),0)
                                        FROM invitations inv
                                        WHERE inv.ivts_RsvpStatus = 'Iterasi'
                                        and inv.ivts_Client_Id = inv2.ivts_Client_Id
                                        and inv.ivts_Uuid = inv2.ivts_Uuid
                                    ) as tot
                                ")
                                : DB::raw("
                                    COALESCE(SUM(ivts_GuestAtt),0) +
                                    (
                                        SELECT COALESCE(SUM(ivts_GuestAtt),0)
                                        FROM invitations inv
                                        WHERE inv.ivts_RsvpStatus = 'Iterasi'
                                        and inv.ivts_Client_Id = inv2.ivts_Client_Id
                                    ) as tot
                                ")
                            )
                            ->where('ivts_GuestAtt', '>', 0)
                            ->first()
                            ->tot,
                        'PRED_RSVP' => (int)(clone $dataRSVP)->select(DB::raw('SUM(ivts_Guest) as tot'))->first()->tot,
                    ],
                    'SOUVENIR' => [
                        'CONFIRM_RSVP' => (int)(clone $dataRSVP)->select(
                            $req->has('filter') && !empty($req->filter)
                                ? DB::raw("
                                    COALESCE(SUM(ivts_SouvenirAtt),0) +
                                    (
                                        SELECT COALESCE(SUM(ivts_SouvenirAtt),0)
                                        FROM invitations inv
                                        WHERE inv.ivts_RsvpStatus = 'Iterasi'
                                        and inv.ivts_Client_Id = inv2.ivts_Client_Id
                                        and inv.ivts_Uuid = inv2.ivts_Uuid
                                    ) as tot
                                ")
                                : DB::raw("
                                    COALESCE(SUM(ivts_SouvenirAtt),0) +
                                    (
                                        SELECT COALESCE(SUM(ivts_SouvenirAtt),0)
                                        FROM invitations inv
                                        WHERE inv.ivts_RsvpStatus = 'Iterasi'
                                        and inv.ivts_Client_Id = inv2.ivts_Client_Id
                                    ) as tot
                                ")
                            )
                            // ->where('ivts_SouvenirAtt', '>', 0)
                            ->first()
                            ->tot,
                        'PRED_RSVP' => (int)(clone $dataRSVP)->select(DB::raw('SUM(ivts_Souvenir) as tot'))->first()->tot,
                    ],
                    'ANGPAU' => [
                        'CONFIRM_RSVP' => (int)(clone $dataRSVP)->select(
                            $req->has('filter') && !empty($req->filter)
                                ? DB::raw("
                                    COALESCE(SUM(ivts_AngpauAtt),0) +
                                    (
                                        SELECT COALESCE(SUM(ivts_AngpauAtt),0)
                                        FROM invitations inv
                                        WHERE inv.ivts_RsvpStatus = 'Iterasi'
                                        and inv.ivts_Client_Id = inv2.ivts_Client_Id
                                        and inv.ivts_Uuid = inv2.ivts_Uuid
                                    ) as tot
                                ")
                                : DB::raw("
                                    COALESCE(SUM(ivts_AngpauAtt),0) +
                                    (
                                        SELECT COALESCE(SUM(ivts_AngpauAtt),0)
                                        FROM invitations inv
                                        WHERE inv.ivts_RsvpStatus = 'Iterasi'
                                        and inv.ivts_Client_Id = inv2.ivts_Client_Id
                                    ) as tot
                                ")
                            )
                            // ->where('ivts_AngpauAtt', '>', 0)
                            ->first()
                            ->tot,
                        'PRED_RSVP' => (int)(clone $dataRSVP)->select(DB::raw('SUM(ivts_Angpau) as tot'))->first()->tot,
                    ]
                ],
                'MANUAL' => [
                    'HADIR' => [
                        'CONFIRM_RSVP' => (clone $dataManual)->select(DB::raw('COALESCE(SUM(ivts_GuestAtt),0) as tot'))->where('ivts_GuestAtt', '>', 0)->count(),
                        'PRED_RSVP' => (clone $dataManual)->count()
                    ],
                    'GUEST' => [
                        'CONFIRM_RSVP' => (int)(clone $dataManual)->select(
                            DB::raw("
                                COALESCE(SUM(ivts_GuestAtt),0) +
                                (
                                    SELECT COALESCE(SUM(ivts_GuestAtt),0)
                                    FROM invitations inv
                                    WHERE inv.ivts_RsvpStatus = 'Iterasi'
                                    and inv.ivts_Client_Id = inv2.ivts_Client_Id
                                    and inv.ivts_Uuid = inv2.ivts_Uuid
                                ) as tot
                            "))
                            ->where('ivts_GuestAtt', '>', 0)
                            ->first()
                            ->tot,
                        'PRED_RSVP' => (int)(clone $dataManual)->select(DB::raw('SUM(ivts_Guest) as tot'))->first()->tot,
                    ],
                    'SOUVENIR' => [
                        'CONFIRM_RSVP' => (int)(clone $dataManual)->select(
                            DB::raw("
                                COALESCE(SUM(ivts_SouvenirAtt),0) +
                                (
                                    SELECT COALESCE(SUM(ivts_SouvenirAtt),0)
                                    FROM invitations inv
                                    WHERE inv.ivts_RsvpStatus = 'Iterasi'
                                    and inv.ivts_Client_Id = inv2.ivts_Client_Id
                                    and inv.ivts_Uuid = inv2.ivts_Uuid
                                ) as tot
                            "))
                            ->where('ivts_SouvenirAtt', '>', 0)
                            ->first()
                            ->tot,
                        'PRED_RSVP' => (int)(clone $dataManual)->select(DB::raw('SUM(ivts_Souvenir) as tot'))->first()->tot,
                    ],
                    'ANGPAU' => [
                        'CONFIRM_RSVP' => (int)(clone $dataManual)->select(
                            DB::raw("
                                COALESCE(SUM(ivts_AngpauAtt),0) +
                                (
                                    SELECT COALESCE(SUM(ivts_AngpauAtt),0)
                                    FROM invitations inv
                                    WHERE inv.ivts_RsvpStatus = 'Iterasi'
                                    and inv.ivts_Client_Id = inv2.ivts_Client_Id
                                    and inv.ivts_Uuid = inv2.ivts_Uuid
                                ) as tot
                            "))
                            ->where('ivts_AngpauAtt', '>', 0)
                            ->first()
                            ->tot,
                        'PRED_RSVP' => (int)(clone $dataManual)->select(DB::raw('SUM(ivts_Angpau) as tot'))->first()->tot,
                    ]
                ],
                'TOTAL' => [
                    'HADIR' => [
                        'CONFIRM_RSVP' => (clone $dataTotal)->select(DB::raw('COALESCE(SUM(ivts_GuestAtt),0) as tot'))->where('ivts_GuestAtt', '>', 0)->count(),
                        'PRED_RSVP' => (clone $dataTotal)->count()
                    ],
                    'GUEST' => [
                        'CONFIRM_RSVP' => (int)(clone $dataTotal)->select(
                            $req->has('filter') && !empty($req->filter)
                                ? DB::raw("
                                    COALESCE(SUM(ivts_GuestAtt),0) +
                                    (
                                        SELECT COALESCE(SUM(ivts_GuestAtt),0)
                                        FROM invitations inv
                                        WHERE inv.ivts_RsvpStatus = 'Iterasi'
                                        and inv.ivts_Client_Id = inv2.ivts_Client_Id
                                        and inv.ivts_Uuid = inv2.ivts_Uuid
                                    ) as tot
                                ")
                                : DB::raw("
                                    COALESCE(SUM(ivts_GuestAtt),0) +
                                    (
                                        SELECT COALESCE(SUM(ivts_GuestAtt),0)
                                        FROM invitations inv
                                        WHERE inv.ivts_RsvpStatus = 'Iterasi'
                                        and inv.ivts_Client_Id = inv2.ivts_Client_Id
                                    ) as tot
                                ")
                            )
                            ->where('ivts_GuestAtt', '>', 0)
                            ->first()
                            ->tot,
                        'PRED_RSVP' => (int)(clone $dataTotal)->select(DB::raw('SUM(ivts_Guest) as tot'))->first()->tot,
                    ],
                    'SOUVENIR' => [
                        'CONFIRM_RSVP' => (int)(clone $dataTotal)->select(
                            $req->has('filter') && !empty($req->filter)
                                ? DB::raw("
                                    COALESCE(SUM(ivts_SouvenirAtt),0) +
                                    (
                                        SELECT COALESCE(SUM(ivts_SouvenirAtt),0)
                                        FROM invitations inv
                                        WHERE inv.ivts_RsvpStatus = 'Iterasi'
                                        and inv.ivts_Client_Id = inv2.ivts_Client_Id
                                        and inv.ivts_Uuid = inv2.ivts_Uuid
                                    ) as tot
                                ")
                                : DB::raw("
                                    COALESCE(SUM(ivts_SouvenirAtt),0) +
                                    (
                                        SELECT COALESCE(SUM(ivts_SouvenirAtt),0)
                                        FROM invitations inv
                                        WHERE inv.ivts_RsvpStatus = 'Iterasi'
                                        and inv.ivts_Client_Id = inv2.ivts_Client_Id
                                    ) as tot
                                ")
                            )
                            ->where('ivts_SouvenirAtt', '>', 0)
                            ->first()
                            ->tot,
                        'PRED_RSVP' => (int)(clone $dataTotal)->select(DB::raw('SUM(ivts_Souvenir) as tot'))->first()->tot,
                    ],
                    'ANGPAU' => [
                        'CONFIRM_RSVP' => (int)(clone $dataTotal)->select(
                            $req->has('filter') && !empty($req->filter)
                                ? DB::raw("
                                    COALESCE(SUM(ivts_AngpauAtt),0) +
                                    (
                                        SELECT COALESCE(SUM(ivts_AngpauAtt),0)
                                        FROM invitations inv
                                        WHERE inv.ivts_RsvpStatus = 'Iterasi'
                                        and inv.ivts_Client_Id = inv2.ivts_Client_Id
                                        and inv.ivts_Uuid = inv2.ivts_Uuid
                                    ) as tot
                                ")
                                : DB::raw("
                                    COALESCE(SUM(ivts_AngpauAtt),0) +
                                    (
                                        SELECT COALESCE(SUM(ivts_AngpauAtt),0)
                                        FROM invitations inv
                                        WHERE inv.ivts_RsvpStatus = 'Iterasi'
                                        and inv.ivts_Client_Id = inv2.ivts_Client_Id
                                    ) as tot
                                ")
                            )
                            ->where('ivts_AngpauAtt', '>', 0)
                            ->first()
                            ->tot,
                        'PRED_RSVP' => (int)(clone $dataTotal)->select(DB::raw('SUM(ivts_Angpau) as tot'))->first()->tot,
                    ]
                ]
            ];

            return response()->json( [
                'status' => true,
                'label' => 'Dashboard found',
                'data' => $hasil
            ], 200);
        } catch (\Throwable $th) {
            return response()->json( [
                'status' => false,
                'label' => $th->getMessage(),
                'data' => [
                    'entity' => 'dashboardCard',
                    'action' => 'create',
                    'result' => $th->getMessage()
                ]
            ], 422);
        }
    }

    /**
        * @OA\Post(
        *     path="/ori/datatableIvts",
        *     operationId="/ori/invitation/datatableIvts",
        *     tags={"Invitations"},
        *     @OA\Parameter(
        *         name="client_id",
        *         in="query",
        *         description="Wedding client ID",
        *         required=true,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Parameter(
        *         name="is_iterasi",
        *         in="query",
        *         description="Iterasi fetch data only",
        *         required=false,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Parameter(
        *         name="qrCode",
        *         in="query",
        *         description="QR Code guest",
        *         required=false,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Parameter(
        *         name="pagination",
        *         in="query",
        *         description="Pagination list",
        *         required=false,
        *         @OA\Schema
        *         (
        *           type="object",
        *           @OA\Property(
        *               property="pagination",
        *               type="object",
        *               @OA\Property(property="rowsPerPage", type="integer"),
        *               @OA\Property(property="page", type="integer")
        *           )
        *         )
        *     ),
        *     @OA\Parameter(
        *         name="column",
        *         in="query",
        *         description="Column by filter",
        *         required=false,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Parameter(
        *         name="filter",
        *         in="query",
        *         description="Filter name guest",
        *         required=false,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Response(
        *         response="200",
        *         description="Return",
        *         @OA\JsonContent(
        *              type="object",
        *              @OA\Property(property="status", type="boolean"),
        *              @OA\Property(property="data", type="array", @OA\Items()),
        *              @OA\Property(property="message", type="string")
        *         )
        *     ),
        *     @OA\Response(
        *         response="422",
        *         description="Error: there is an error on the server / backend",
        *          @OA\JsonContent(
        *              type="object",
        *              @OA\Property(property="status", type="boolean"),
        *              @OA\Property(property="message", type="string"),
        *         )
        *     ),
        * )
    */
    public function datatableIvts(Request $req)
    {
        $this->validate($req, [
            'client_id' => 'required|string'
            // 'pagination.rowsPerPage' => 'required|integer',
            // 'pagination.page' => 'required|integer'
        ]);

        try {
            if ($req->has('is_iterasi') && $req->is_iterasi == 1) {
                $select = ['*'];
            } else {
                $select = [
                    'ivts_Id',
                    'ivts_Client_Id',
                    'ivts_Uuid',
                    'ivts_Name',
                    DB::raw('MAX(ivts_NoHp) as ivts_NoHp'),
                    DB::raw('MAX(ivts_Address) as ivts_Address'),
                    DB::raw('MAX(ivts_Guest) as ivts_Guest'),
                    DB::raw("COALESCE(SUM(ivts_GuestAtt),0) as ivts_GuestAtt"),
                    DB::raw('MAX(ivts_GuestAttTime) as ivts_GuestAttTime'),
                    DB::raw('MAX(ivts_GuestAttCounter) as ivts_GuestAttCounter'),
                    DB::raw('MAX(ivts_Souvenir) as ivts_Souvenir'),
                    DB::raw('COALESCE(SUM(ivts_SouvenirAtt),0) as ivts_SouvenirAtt'),
                    DB::raw('MAX(ivts_SouvenirAttTime) as ivts_SouvenirAttTime'),
                    DB::raw('MAX(ivts_SouvenirAttCounter) as ivts_SouvenirAttCounter'),
                    DB::raw('MAX(ivts_Angpau) as ivts_Angpau'),
                    DB::raw('COALESCE(SUM(ivts_AngpauAtt),0) as ivts_AngpauAtt'),
                    DB::raw('MAX(ivts_AngpauAttTime) as ivts_AngpauAttTime'),
                    DB::raw('MAX(ivts_AngpauAttCounter) as ivts_AngpauAttCounter'),
                    DB::raw('MAX(ivts_Category) as ivts_Category'),
                    DB::raw('MAX(ivts_Group) as ivts_Group'),
                    DB::raw('MAX(ivts_GroupFam) as ivts_GroupFam'),
                    DB::raw('MAX(ivts_GroupSub) as ivts_GroupSub'),
                    DB::raw('MAX(ivts_Seat) as ivts_Seat'),
                    DB::raw('MAX(ivts_Selfie) as ivts_Selfie'),
                    DB::raw('MAX(ivts_SelfieFile) as ivts_SelfieFile'),
                    DB::raw('MAX(ivts_SelfieTime) as ivts_SelfieTime'),
                    DB::raw('MAX(ivts_capt_Id) as ivts_capt_Id'),
                    DB::raw('MAX(ivts_tplqr_Id) as ivts_tplqr_Id'),
                    DB::raw('MAX(ivts_tplivts_Id) as ivts_tplivts_Id'),
                    DB::raw('MAX(ivts_CounterAtt) as ivts_CounterAtt'),
                    DB::raw('MAX(ivts_Files) as ivts_Files'),
                    DB::raw('MAX(ivts_FilesIvts) as ivts_FilesIvts'),
                    DB::raw('MAX(ivts_RsvpStatus) as ivts_RsvpStatus'),
                    DB::raw('MAX(ivts_RsvpGuest) as ivts_RsvpGuest'),
                    DB::raw('MAX(ivts_RsvpSendCount) as ivts_RsvpSendCount'),
                    DB::raw('MAX(ivts_RsvpMessage) as ivts_RsvpMessage'),
                    DB::raw('MAX(ivts_RsvpTime) as ivts_RsvpTime'),
                    DB::raw('MAX(ivts_LinkExternal) as ivts_LinkExternal'),
                    DB::raw('MAX(ivts_SentTime) as ivts_SentTime'),
                    DB::raw('MAX(ivts_SentStatus) as ivts_SentStatus'),
                    DB::raw('MAX(ivts_Created) as ivts_Created'),
                    DB::raw('MAX(ivts_Updated) as ivts_Updated'),
                    DB::raw('MAX(ivts_Deleted) as ivts_Deleted'),
                    DB::raw("
                    CASE WHEN MAX(ivts_RsvpRespond) = 0 THEN 'Tidak Hadir'
                        WHEN MAX(ivts_RsvpRespond) = 1 THEN CONCAT('Hadir', '(', MAX(ivts_RsvpGuest), ')')
                        ELSE 'Belum Konfirmasi'
                    END AS ivts_RsvpRespond
                ")
                ];
            }

            $data = Invitations::select($select)
            ->where('ivts_Client_Id', $req->client_id)
            ->groupBy(
                'ivts_Client_Id',
                'ivts_Uuid',
                'ivts_Name'
            );

            // $data->whereIn('ivts_RsvpStatus', ['Undangan', 'RSVP', 'Iterasi']);
            $data->where('ivts_RsvpStatus', '<>', 'Iterasi');

            if ($req->has('qrCode') && !empty($req->qrCode)) {
                $data->where('ivts_Uuid', $req->qrCode);
            }

            if ($req->has('column') && !empty($req->column)) {
                if ($req->has('filter') && !empty($req->filter)) {
                    $data->where($req->column, 'like', '%'.$req->filter.'%');
                }
            } else {
                if ($req->has('filter') && !empty($req->filter)) {
                    $data->where('ivts_Name', 'like', '%'.$req->filter.'%');
                }
            }

            if ($req->has('pagination')) {
                $rows = (int)$req['pagination']['rowsPerPage'] > 0
                    ? $req['pagination']['rowsPerPage']
                    : count($data->get()->toArray());

                $page = (int)$req['pagination']['page'] > 0
                    ? $req['pagination']['page']
                    : 1;

                // return count($data->get()->toArray());
                // return [$rows, $page];
                if (!$req->has('is_iterasi') && $req->is_iterasi != 1) {
                    $combineIterasi = $this->viewIterasiList($data->get()->toArray());

                    $convertData = collect($combineIterasi);

                    return response()->json( [
                        'status' => true,
                        'label' => 'Data found !',
                        'data' => [
                            'table' => $convertData->paginate($rows, $page),
                            'dashboards' => $this->dashboardCard(new Request($req->all()))->original['data']
                        ]
                    ], 200);
                }

                return response()->json( [
                    'status' => true,
                    'label' => 'Data found !',
                    'data' => [
                        'table' => $data->paginate($rows, ['*'], 'page', $page),
                        'dashboards' => $this->dashboardCard(new Request($req->all()))->original['data']
                    ]
                ], 200);
            } else {
                if (!$req->has('is_iterasi') && $req->is_iterasi != 1) {
                    $combineIterasi = $this->viewIterasiList($data->get()->toArray());

                    return response()->json( [
                        'status' => true,
                        'label' => 'Data found !',
                        'data' => [
                            'table' => $combineIterasi,
                            'dashboards' => $this->dashboardCard(new Request($req->all()))->original['data']
                        ]
                    ], 200);
                }

                return response()->json( [
                    'status' => true,
                    'label' => 'Data found !',
                    'data' => [
                        'table' => $data->get(),
                        'dashboards' => $this->dashboardCard(new Request($req->all()))->original['data']
                    ]
                ], 200);
            }
        } catch (\Throwable $th) {
            return response()->json( [
                'status' => false,
                'label' => $th->getMessage(),
                'data' => [
                    'entity' => 'datatableIvts',
                    'action' => 'create',
                    'result' => $th->getMessage()
                ]
            ], 422);
        }
    }

    public function viewIterasiList($data)
    {
        $joinIterasi = [];
        foreach ($data as $key => $value) {
            $joinIterasi[] = array_merge($value, [
                'iterasi_list' => Invitations::where('ivts_Client_Id', $value['ivts_Client_Id'])
                    ->where('ivts_Uuid', $value['ivts_Uuid'])
                    ->orderBy('ivts_GuestAttTime')
                    ->get()
                    ->toArray()
            ]);
        }

        return $joinIterasi;
    }
}
