<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

// Model
use App\Models\Settings\Category;

class CategoryController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'cat_name' => 'required|alpha_dash|unique:rsvp_category_mstr,cat_name'
        ]);

        try {
            $insert = Category::updateOrCreate([
                'cat_name' => $request->cat_name
            ], $request);
        } catch (\Throwable $th) {
            return response()->json( [
                'status' => false,
                'label' => $th->getMessage(),
                'data' => [
                    'entity' => 'search_tamu',
                    'action' => 'create',
                    'result' => $th->getMessage()
                ]
            ], 409);
        }
    }
}
