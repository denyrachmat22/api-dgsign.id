<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

// Models
use App\Models\Settings\Menu;

class MenuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login','register']]);
    }

    public function store(Request $req)
    {
        $this->validate($req, [
            'menus_name' => 'required',
            'menus_url' => 'required|unique:rsvp_menus,menus_url'
        ]);

        try
        {
            $getID = Menu::orderBy('menus_id', 'desc')->first();

            if (empty($getID)) {
                $id = 'A0001';
            } else {
                $id = substr($getID->menus_id, 0, 1).sprintf('%04d', (int) substr($getID->menus_id, -3) + 1);
            }

            $insert = Menu::create([
                'menus_id' => $id,
                'menus_name' => $req->menus_name,
                'menus_url' => $req->menus_url,
                'menus_parent' => $req->menus_parent,
            ]);

            return [
                'status' => true,
                'label' => 'Menu successfully inserted !',
                'data' => $insert
            ];
        } catch (\Exception $e)
        {
            return response()->json( [
                       'entity' => 'menus',
                       'action' => 'create',
                       'result' => 'failed'
            ], 409);
        }
    }
}
