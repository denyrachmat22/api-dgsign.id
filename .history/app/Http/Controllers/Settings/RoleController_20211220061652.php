<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

// Models
use App\Models\Settings\Role;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login','register']]);
    }

    public function store(Request $req)
    {
        $this->validate($req, [
            'roles_name' => 'required|unique:rsvp_roles_mstr,roles_name'
        ]);

        try {
            $insert = Role::create($req->all());

            return [
                'status' => true,
                'label' => 'Menu successfully inserted !',
                'data' => $insert
            ];
        } catch (\Throwable $th) {
            return response()->json( [
                'entity' => 'rsvp_roles_mstr',
                'action' => 'create',
                'result' => $th->getMessage()
            ], 409);
        }
    }

    public function storeDet(Request $req)
    {
        $this->validate($req, [
            'roles_mstr_id' => 'required',
            'menus_id' => 'required|array|between:1'
        ]);

        try {
            $insert = Role::create($req->all());

            return [
                'status' => true,
                'label' => 'Menu successfully inserted !',
                'data' => $insert
            ];
        } catch (\Throwable $th) {
            return response()->json( [
                'entity' => 'rsvp_roles_det',
                'action' => 'create',
                'result' => $th->getMessage()
            ], 409);
        }
    }
}
