<?php

namespace App\Http\Controllers\ORI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

// Model
use App\Models\ORI\Invitations;
use App\Models\ORI\User;

class InvitationController extends Controller
{
    public function checkQRCode(Request $req)
    {
        //validate incoming request
        $this->validate($req, [
            'client_id' => 'required|string',
            'qrCode' => 'required|string|exists:mysql_db_old.invitations,ivts_Uuid'
        ],[
           'qrCode.exists' => 'No QR code ID found !!'
        ]);

        try {
            $data = Invitations::where('ivts_Uuid', $req->qrCode);

            if (!empty($req->client_id)) {
                $data->where('ivts_Client_Id', $req->client_id);
            }

            if (!empty($data->first())) {
                return response()->json([
                    'status' => true,
                    'message' => 'Guest Invitation found !!',
                    'data' => $data->first()
                ], 200);
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'Guest Invitation not found !!',
                    'data' => $data->first()
                ], 200);
            }
        } catch (\Throwable $th) {
            return response()->json( [
                'status' => false,
                'label' => $th->getMessage(),
                'data' => [
                    'entity' => 'invitation',
                    'action' => 'create',
                    'result' => $th->getMessage()
                ]
            ], 409);
        }
    }

    public function updateDetail(Request $req)
    {
        //validate incoming request
        $this->validate($req, [
            'client_id' => 'required|string',
            'qrCode' => 'required|string|exists:mysql_db_old.invitations,ivts_Uuid',
            'guest_count' => 'required|integer',
            'souvenir_count' => 'required|integer',
            'angpau_count' => 'required|integer'
        ],[
            'qrCode.exists' => 'No QR Code found in database !'
        ]);

        try {
            //code...
        } catch (\Throwable $th) {
            return response()->json( [
                'status' => false,
                'label' => $th->getMessage(),
                'data' => [
                    'entity' => 'invitation',
                    'action' => 'create',
                    'result' => $th->getMessage()
                ]
            ], 409);
        }
    }
}
