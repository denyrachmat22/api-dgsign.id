<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserDet extends Model {
    protected $primaryKey = 'username';
    protected $table = 'rsvp_roles_mstr';
    protected $fillable = [
        'username',
        'users_det_addr1',
        'users_det_addr2',
        'users_det_phone',
        'users_det_fam_count',
        'users_det_souvenir_count',
        'users_det_angpau_count'
    ];
}
