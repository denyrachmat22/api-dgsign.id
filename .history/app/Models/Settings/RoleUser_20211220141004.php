<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model {
    protected $table = 'rsvp_user_roles';
    protected $fillable = [
        'username',
        'rolse_id'
    ];
}
