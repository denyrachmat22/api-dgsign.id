<?php

namespace App\Http\Controllers\RSVP;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

// Model
use App\Models\RSVP\Location;

class LocationController extends Controller
{
    public function store(Request $req)
    {
        //validate incoming request
        $this->validate($req, [
            'loc_name' => 'required|string'
        ]);

        try {
            $insert = Location::updateOrCreate([
                'id' => $req->id
            ], $req->all());

            return [
                'status' => true,
                'label' => 'Menu successfully inserted !',
                'data' => $insert
            ];
        } catch (\Throwable $th) {
            return response()->json( [
                'status' => false,
                'label' => $th->getMessage(),
                'data' => [
                    'entity' => 'rsvp_loc_mstr',
                    'action' => 'create',
                    'result' => $th->getMessage()
                ]
            ], 409);
        }
    }
}
