<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;

class RoleDet extends Model {
    protected $table = 'rsvp_roles_det';
    protected $fillable = [
        'roles_mstr_id',
        'menus_id'
    ];
}
