<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;

class UserCategory extends Model {
    protected $table = 'rsvp_user_cat';
    protected $fillable = [
        'username',
        'category_id'
    ];

    public function categoryMaster()
    {
        return $this->hasOne('App\Models\Settings\Category','cat_name','category_id');
    }
}
