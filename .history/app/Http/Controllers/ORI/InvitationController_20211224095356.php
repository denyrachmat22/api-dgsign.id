<?php

namespace App\Http\Controllers\ORI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

// Model
use App\Models\ORI\Invitations;
use App\Models\ORI\User;

class InvitationController extends Controller
{
    public function checkQRCode(Request $req)
    {
        //validate incoming request
        $this->validate($req, [
            'client_id' => 'required|string',
            'qrCode' => 'required|string'
        ]);

        try {
            //code...
        } catch (\Throwable $th) {
            return response()->json( [
                'status' => false,
                'label' => $th->getMessage(),
                'data' => [
                    'entity' => 'invitation',
                    'action' => 'create',
                    'result' => $th->getMessage()
                ]
            ], 409);
        }
    }
}
