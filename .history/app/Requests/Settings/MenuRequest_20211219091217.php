<?php
namespace App\Http\Requests\Settings;

use Anik\Form\FormRequest;

class MenuRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'menus_name' => 'required',
            'menus_url' => 'required|unique:rsvp_menus,menu_url'
        ];
    }

    public function messages()
    {
        return [
            'menus_name.required' => 'Menu name is required !',
            'menus_url.required' => 'Menu URL is required !!',
            'menus_url.unique' => 'Menu URL is already added!!'
        ];
    }
}
