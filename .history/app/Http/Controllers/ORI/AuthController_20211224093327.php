<?php

namespace App\Http\Controllers\ORI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

// Model
use App\Models\ORI\User;

class AuthController extends Controller
{
    public function login(Request $req)
    {
         //validate incoming request
         $this->validate($req, [
            'username' => 'required|string',
            'password' => 'required|string',
        ]);

        $dataUser = User::where('admnUsername', $req->username)->first();
        if(!empty($dataUser)) {
            return password_verify($req->password, $dataUser->admnPassword);
        } else {
            return 'not_found';
        }
    }
}
