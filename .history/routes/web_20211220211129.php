<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return 'Welcome to dgsign.id API Service';
});

$router->group(['middleware' => 'auth','prefix' => 'api'], function ($router)
{
    $router->get('me', 'AuthController@me');

    // Menus Manage
    $router->post('menus', 'Settings\MenuController@store');

    // Roles Manage
    $router->post('roles', 'Settings\RoleController@store');
    $router->post('rolesDet', 'Settings\RoleController@storeDet');
    $router->post('rolesUser', 'Settings\RoleController@storeUserMapping');

    // RSVP
    $router->post('cariTamu', 'RSVP\TamuController@searchTamu');
});

$router->group(['prefix' => 'api'], function () use ($router)
{
    // Register Users
    $router->post('register', 'AuthController@register');
    $router->post('registerTamu', 'AuthController@registerTamu');
    $router->post('updateUserDetail', 'AuthController@updateUserDetail');

    $router->post('login', 'AuthController@login');
    $router->get('loginQRCode/{qrID}', 'AuthController@loginViaQR');
});
