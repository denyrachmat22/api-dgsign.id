<?php

namespace App\Http\Controllers\RSVP;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

// Model
use App\Models\UserDet;

class TamuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['searchTamu']]);
    }

    /**
        * @OA\Post(
        *     path="/api/cariTamu",
        *     operationId="/api/rsvp/cariTamu",
        *     tags={"RSVP"},
        *     @OA\Parameter(
        *         name="query_str",
        *         in="query",
        *         description="Search people by name",
        *         required=true,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Response(
        *         response="200",
        *         description="Returns users list from query",
        *         @OA\JsonContent(
        *              type="object",
        *              @OA\Property(property="status", type="boolean"),
        *              @OA\Property(property="data", type="array", @OA\Items()),
        *              @OA\Property(property="label", type="string")
        *         )
        *     ),
        *     @OA\Response(
        *         response="401",
        *         description="Error: query required",
        *          @OA\JsonContent(
        *              type="object",
        *              @OA\Property(property="status", type="boolean"),
        *              @OA\Property(property="message", type="string"),
        *         )
        *     ),
        * )
    */
    public function searchTamu(Request $req)
    {
        //validate incoming request
        $this->validate($req, [
            'query_str' => 'required|string'
        ]);

        try {
            $cariUser = UserDet::where('fullname', 'like', '%'.$req->query_str.'%')->get()->toArray();

            if (count($cariUser) > 0) {
                return [
                    'status' => true,
                    'label' => 'User found !',
                    'data' => $cariUser
                ];
            } else {
                return [
                    'status' => false,
                    'label' => 'User not found !',
                    'data' => $cariUser
                ];
            }
        } catch (\Throwable $th) {
            return response()->json( [
                'status' => false,
                'label' => $th->getMessage(),
                'data' => [
                    'entity' => 'search_tamu',
                    'action' => 'create',
                    'result' => $th->getMessage()
                ]
            ], 409);
        }
    }
}
