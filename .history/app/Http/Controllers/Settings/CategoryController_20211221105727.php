<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

// Model
use App\Models\Settings\Category;

class CategoryController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'cat_name' => 'required|unique:rsvp_category_mstr,cat_name'
        ]);
    }
}
