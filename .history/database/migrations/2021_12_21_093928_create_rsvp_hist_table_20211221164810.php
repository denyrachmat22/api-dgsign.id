<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRsvpHistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rsvp_hist', function (Blueprint $table) {
            $table->id();
            $table->string('username');
            $table->integer('hist_user_fam_count');
            $table->integer('hist_user_souvenir_count');
            $table->integer('hist_user_angpau_count');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rsvp_hist');
    }
}
