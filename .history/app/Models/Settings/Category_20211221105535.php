<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {
    protected $table = 'rsvp_category_mstr';
    protected $fillable = [
        'cat_name',
        'cat_desc',
        'cat_parent_id'
    ];
}
