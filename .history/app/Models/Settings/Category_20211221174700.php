<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {
    protected $table = 'rsvp_category_mstr';
    protected $primaryKey = 'cat_name';
    protected $fillable = [
        'cat_name',
        'cat_desc',
        'cat_parent_id'
    ];

    public function usersCategory()
    {
        return $this->hasMany('App\Models\Settings\UserCategory','category_id','id');
    }
}
