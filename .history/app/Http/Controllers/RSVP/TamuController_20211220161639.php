<?php

namespace App\Http\Controllers\RSVP;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

// Model
use App\Models\User;

class TamuController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth:api', ['except' => ['login','register', 'registerTamu', 'loginViaQR']]);
    }

    public function searchTamu(Request $req)
    {
        //validate incoming request
        $this->validate($req, [
            'query' => 'required|string'
        ]);

        try {
            $cariUser = User::with('userDetail', function ($q) use ($req){
                $q->where('fullname', 'like', $req->query);
            })->whereHas('userDetail', function ($q) use ($req){
                $q->where('fullname', 'like', $req->query);
            })->get();

            return [
                'status' => true,
                'label' => 'User found !',
                'data' => $cariUser->userDetail
            ];
        } catch (\Throwable $th) {
            return response()->json( [
                'status' => false,
                'label' => $th2->getMessage(),
                'data' => [
                    'entity' => 'rsvp_users_det',
                    'action' => 'create',
                    'result' => $th2->getMessage()
                ]
            ], 409);
        }
    }
}
