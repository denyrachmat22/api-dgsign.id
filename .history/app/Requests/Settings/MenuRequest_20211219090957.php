<?php
namespace App\Http\Requests\Settings;

use Anik\Form\FormRequest;

class MenuRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'menu_name' => 'required',
            'menu_url' => 'required|unique:MSTEMP_TBL,MSTEMP_ID'
        ];
    }

    public function messages()
    {
        return [
            'menu_name.required' => 'Menu name is required !',
            'menu_url.required' => 'Menu URL is required !!',
            'menu_url.unique' => 'Username already used!!'
        ];
    }
}
