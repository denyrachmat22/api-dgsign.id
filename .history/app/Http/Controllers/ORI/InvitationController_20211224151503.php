<?php

namespace App\Http\Controllers\ORI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

// Model
use App\Models\ORI\Invitations;
use App\Models\ORI\User;

class InvitationController extends Controller
{
    /**
        * @OA\Post(
        *     path="/old/scanQRCode",
        *     operationId="/old/invitation/scanQRCode",
        *     tags={"Invitations"},
        *     @OA\Parameter(
        *         name="client_id",
        *         in="query",
        *         description="Wedding client ID",
        *         required=true,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Parameter(
        *         name="qrCode",
        *         in="query",
        *         description="QR Code ID",
        *         required=true,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Response(
        *         response="200",
        *         description="Return",
        *         @OA\JsonContent(
        *              type="object",
        *              @OA\Property(property="status", type="boolean"),
        *              @OA\Property(property="data", type="array", @OA\Items()),
        *              @OA\Property(property="message", type="string")
        *         )
        *     ),
        *     @OA\Response(
        *         response="422",
        *         description="Error: Some validation not passed",
        *          @OA\JsonContent(
        *              type="array",
        *              @OA\Items()
        *         )
        *     ),
        *     @OA\Response(
        *         response="401",
        *         description="Error: username or password not right",
        *          @OA\JsonContent(
        *              type="object",
        *              @OA\Property(property="status", type="boolean"),
        *              @OA\Property(property="message", type="string"),
        *         )
        *     ),
        * )
    */
    public function checkQRCode(Request $req)
    {
        //validate incoming request
        $this->validate($req, [
            'client_id' => 'required|string',
            'qrCode' => 'required|string|exists:mysql_db_old.invitations,ivts_Uuid'
        ],[
           'qrCode.exists' => 'No QR code ID found !!'
        ]);

        try {
            $data = Invitations::where('ivts_Uuid', $req->qrCode);

            if (!empty($req->client_id)) {
                $data->where('ivts_Client_Id', $req->client_id);
            }

            if (!empty($data->first())) {
                return response()->json([
                    'status' => true,
                    'message' => 'Guest Invitation found !!',
                    'data' => $data->first()
                ], 200);
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'Guest Invitation not found !!',
                    'data' => $data->first()
                ], 200);
            }
        } catch (\Throwable $th) {
            return response()->json( [
                'status' => false,
                'label' => $th->getMessage(),
                'data' => [
                    'entity' => 'invitation',
                    'action' => 'create',
                    'result' => $th->getMessage()
                ]
            ], 409);
        }
    }

    /**
        * @OA\Post(
        *     path="/old/guestConfirmation",
        *     operationId="/old/invitation/guestConfirmation",
        *     tags={"Invitations"},
        *     @OA\Parameter(
        *         name="client_id",
        *         in="query",
        *         description="Wedding client ID",
        *         required=true,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Parameter(
        *         name="guest_count",
        *         in="query",
        *         description="Guest count",
        *         required=true,
        *         @OA\Schema(type="integer")
        *     ),
        *     @OA\Parameter(
        *         name="souvenir_count",
        *         in="query",
        *         description="Souvenir get count",
        *         required=true,
        *         @OA\Schema(type="integer")
        *     ),
        *     @OA\Parameter(
        *         name="angpau_count",
        *         in="query",
        *         description="Angpau given count",
        *         required=true,
        *         @OA\Schema(type="integer")
        *     ),
        *     @OA\Parameter(
        *         name="operator",
        *         in="query",
        *         description="Operator logged in",
        *         required=false,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Response(
        *         response="200",
        *         description="Return",
        *         @OA\JsonContent(
        *              type="object",
        *              @OA\Property(property="status", type="boolean"),
        *              @OA\Property(property="data", type="array", @OA\Items()),
        *              @OA\Property(property="message", type="string")
        *         )
        *     ),
        *     @OA\Response(
        *         response="422",
        *         description="Error: Some validation not passed",
        *          @OA\JsonContent(
        *              type="array",
        *              @OA\Items()
        *         )
        *     ),
        *     @OA\Response(
        *         response="401",
        *         description="Error: username or password not right",
        *          @OA\JsonContent(
        *              type="object",
        *              @OA\Property(property="status", type="boolean"),
        *              @OA\Property(property="message", type="string"),
        *         )
        *     ),
        * )
    */
    public function guestConfirmation(Request $req)
    {
        //validate incoming request
        $this->validate($req, [
            'client_id' => 'required|string',
            'guest_count' => 'required|integer',
            'souvenir_count' => 'required|integer',
            'angpau_count' => 'required|integer'
        ],[
            'qrCode.exists' => 'No QR Code found in database !'
        ]);

        try {
            Invitations::where('ivts_Uuid', $req->qrCode)->update([
                'ivts_GuestAtt' => $req->guest_count,
                'ivts_GuestAttTime' => date('Y-m-d H:i:s'),
                'ivts_GuestAttCounter' => $req->operator,
                'ivts_Souvenir' => $req->souvenir_count,
                'ivts_SouvenirAtt' => date('Y-m-d H:i:s'),
                'ivts_SouvenirAttCounter' => $req->operator,
                'ivts_Angpau' => $req->angpau_count,
                'ivts_AngpauAtt' => date('Y-m-d H:i:s'),
                'ivts_AngpauAttCounter' => $req->operator,
            ]);

            return response()->json( [
                'status' => true,
                'label' => 'Invitation updated !',
                'data' => Invitations::where('ivts_Uuid', $req->qrCode)->get()->toArray()
            ], 200);
        } catch (\Throwable $th) {
            return response()->json( [
                'status' => false,
                'label' => $th->getMessage(),
                'data' => [
                    'entity' => 'invitation',
                    'action' => 'create',
                    'result' => $th->getMessage()
                ]
            ], 409);
        }
    }

    /**
        * @OA\Post(
        *     path="/old/guestRegister",
        *     operationId="/old/invitation/guestRegister",
        *     tags={"Invitations"},
        *     @OA\Parameter(
        *         name="client_id",
        *         in="query",
        *         description="Wedding client ID",
        *         required=true,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Parameter(
        *         name="guest_name",
        *         in="query",
        *         description="Guest name",
        *         required=true,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Parameter(
        *         name="guest_addr",
        *         in="query",
        *         description="Guest Address / Instance",
        *         required=true,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Parameter(
        *         name="guest_count",
        *         in="query",
        *         description="Guest count",
        *         required=true,
        *         @OA\Schema(type="integer")
        *     ),
        *     @OA\Parameter(
        *         name="souvenir_count",
        *         in="query",
        *         description="Souvenir get count",
        *         required=true,
        *         @OA\Schema(type="integer")
        *     ),
        *     @OA\Parameter(
        *         name="angpau_count",
        *         in="query",
        *         description="Angpau given count",
        *         required=true,
        *         @OA\Schema(type="integer")
        *     ),
        *     @OA\Parameter(
        *         name="operator",
        *         in="query",
        *         description="Operator logged in",
        *         required=false,
        *         @OA\Schema(type="string")
        *     ),
        *     @OA\Response(
        *         response="200",
        *         description="Return",
        *         @OA\JsonContent(
        *              type="object",
        *              @OA\Property(property="status", type="boolean"),
        *              @OA\Property(property="data", type="array", @OA\Items()),
        *              @OA\Property(property="message", type="string")
        *         )
        *     ),
        *     @OA\Response(
        *         response="422",
        *         description="Error: Some validation not passed",
        *          @OA\JsonContent(
        *              type="array",
        *              @OA\Items()
        *         )
        *     ),
        *     @OA\Response(
        *         response="409",
        *         description="Error: there is an error on the server / backend",
        *          @OA\JsonContent(
        *              type="object",
        *              @OA\Property(property="status", type="boolean"),
        *              @OA\Property(property="message", type="string"),
        *         )
        *     ),
        * )
    */
    public function guestRegister(Request $req)
    {
        //validate incoming request
        $this->validate($req, [
            'client_id' => 'required|string',
            'guest_name' => 'required|string',
            'guest_addr' => 'required|string',
            'guest_count' => 'required|integer',
            'souvenir_count' => 'required|integer',
            'angpau_count' => 'required|integer'
        ]);

        try {
            $id = $this->checkID('');
            $data = Invitations::create([
                'ivts_Guest' => 2,
                'ivts_Client_Id' => $req->client_id,
                'ivts_Uuid' => $id,
                'ivts_Name' => $req->guest_name,
                'ivts_Address' => $req->guest_addr,
                'ivts_GuestAtt' => $req->guest_count,
                'ivts_GuestAttTime' => date('Y-m-d H:i:s'),
                'ivts_GuestAttCounter' => $req->operator,
                'ivts_Souvenir' => $req->souvenir_count,
                'ivts_SouvenirAtt' => date('Y-m-d H:i:s'),
                'ivts_SouvenirAttCounter' => $req->operator,
                'ivts_Angpau' => $req->angpau_count,
                'ivts_AngpauAtt' => date('Y-m-d H:i:s'),
                'ivts_AngpauAttCounter' => $req->operator
            ]);

            return response()->json( [
                'status' => true,
                'label' => 'Guest added !',
                'data' => $data
            ], 200);
        } catch (\Throwable $th) {
            return response()->json( [
                'status' => false,
                'label' => $th->getMessage(),
                'data' => [
                    'entity' => 'invitation',
                    'action' => 'create',
                    'result' => $th->getMessage()
                ]
            ], 409);
        }
    }

    public function checkID($id)
    {
        if (empty($id)) {
            $ids = $this->generateRandomString(8).'-'.$this->generateRandomString(4).'-'.$this->generateRandomString(4).'-'.$this->generateRandomString(4).'-'.$this->generateRandomString(12);

            $cekData = Invitations::where('ivts_Uuid', $ids)->first();

            if (empty($cekData)) {
                return $ids;
            } else {
                return $this->checkID('');
            }
        } else {
            $cekData = Invitations::where('ivts_Uuid', $id)->first();
            if (empty($cekData)) {
                return $id;
            } else {
                return $this->checkID('');
            }
        }
    }

    public function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function searchGuest(Request $req)
    {
        //validate incoming request
        $this->validate($req, [
            'query_search' => 'required|string'
        ]);

        try {
            $data = Invitations::where('ivts_name', 'like', '%'.$req->query_serach.'%')->get()->toArray();
            return response()->json( [
                'status' => true,
                'label' => 'Search found !',
                'data' => $data
            ], 200);
        } catch (\Throwable $th) {
            return response()->json( [
                'status' => false,
                'label' => $th->getMessage(),
                'data' => [
                    'entity' => 'invitation',
                    'action' => 'create',
                    'result' => $th->getMessage()
                ]
            ], 409);
        }
    }
}
