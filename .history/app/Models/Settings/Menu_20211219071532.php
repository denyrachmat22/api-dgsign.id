<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model {
    protected $table = 'rsvp_menus';
}
