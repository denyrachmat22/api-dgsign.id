<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;

class Role extends Model {
    protected $table = 'rsvp_roles_mstr';
    protected $fillable = [
        'roles_name',
        'roles_desc'
    ];
}
