<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model {
    protected $table = 'rsvp_menus';
    protected $fillable = [
        'menus_id',
        'menus_name',
        'menus_url',
        'menus_parent',
    ];
}
