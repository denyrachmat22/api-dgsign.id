<?php

namespace App\Http\Controllers\ORI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

// Model
use App\Models\ORI\Invitations;
use App\Models\ORI\User;

class InvitationController extends Controller
{
    public function login(Request $req)
    {
         //validate incoming request
         $this->validate($req, [
            'username' => 'required|string',
            'password' => 'required|string',
        ]);
    }
}
