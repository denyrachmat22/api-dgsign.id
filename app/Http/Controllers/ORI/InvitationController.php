<?php

namespace App\Http\Controllers\ORI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

// Model
use App\Models\ORI\Invitations;
use App\Models\ORI\User;

use Illuminate\Log\Logger;

// Config -> file .env file dan config/database.php
// -mysql_db_old -> ini ke database dgsign_dev
// -mysql_db_old -> ini ke database dgsign_live
class InvitationController extends Controller
{
    /**
     * @OA\Post(
     *     path="/ori/scanQRCode",
     *     operationId="/ori/invitation/scanQRCode",
     *     tags={"Invitations"},
     *     @OA\Parameter(
     *         name="client_id",
     *         in="query",
     *         description="Wedding client ID",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="qrCode",
     *         in="query",
     *         description="QR Code ID",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Return",
     *         @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="status", type="boolean"),
     *              @OA\Property(property="data", type="array", @OA\Items()),
     *              @OA\Property(property="message", type="string")
     *         )
     *     ),
     *     @OA\Response(
     *         response="422",
     *         description="Error: Some validation not passed",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items()
     *         )
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Error: username or password not right",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="status", type="boolean"),
     *              @OA\Property(property="message", type="string"),
     *         )
     *     ),
     * )
     */
    public function checkQRCode(Request $req)
    {
        //validate incoming request
        $this->validate($req, [
            'client_id' => 'required|string',
            'qrCode' => 'required|string|exists:mysql_db_old.invitations,ivts_Uuid'
        ], [
            'qrCode.exists' => 'No QR code ID found !!'
        ]);

        try {
            $data = Invitations::select(
                'ivts_Client_Id',
                'ivts_Uuid',
                'ivts_Name',
                'ivts_Address',
                DB::raw('COUNT(*) AS ivts_TotalAtt'),
                DB::raw('MAX(ivts_NoHp) AS ivts_NoHp'),
                DB::raw('MAX(ivts_Guest) AS ivts_Guest'),
                DB::raw('COALESCE(SUM(ivts_GuestAtt),0) AS ivts_GuestAtt'),
                DB::raw('MAX(ivts_GuestAttTime) AS ivts_GuestAttTime'),
                DB::raw('MAX(ivts_GuestAttCounter) AS ivts_GuestAttCounter'),
                DB::raw('MAX(ivts_Souvenir) AS ivts_Souvenir'),
                DB::raw('COALESCE(SUM(ivts_SouvenirAtt),0) AS ivts_SouvenirAtt'),
                DB::raw('MAX(ivts_SouvenirAttTime) AS ivts_SouvenirAttTime'),
                DB::raw('MAX(ivts_SouvenirAttCounter) AS ivts_SouvenirAttCounter'),
                DB::raw('MAX(ivts_Angpau) AS ivts_Angpau'),
                DB::raw('COALESCE(SUM(ivts_AngpauAtt),0) AS ivts_AngpauAtt'),
                DB::raw('MAX(ivts_AngpauAttTime) AS ivts_AngpauAttTime'),
                DB::raw('MAX(ivts_AngpauAttCounter) AS ivts_AngpauAttCounter'),
                DB::raw('MAX(ivts_Gift) AS ivts_GiftOld'),
                DB::raw('COALESCE(SUM(ivts_GiftAtt),0) AS ivts_GiftAttOld'),
                DB::raw('MAX(ivts_GiftDesc) AS ivts_Gift'),
                DB::raw('MAX(ivts_GiftAttTime) AS ivts_GiftAttTime'),
                DB::raw('MAX(ivts_GiftAttCounter) AS ivts_GiftAttCounter'),
                DB::raw('MAX(ivts_Category) AS ivts_Category'),
                DB::raw('MAX(ivts_Group) AS ivts_Group'),
                DB::raw('MAX(ivts_GroupFam) AS ivts_GroupFam'),
                DB::raw('MAX(ivts_GroupSub) AS ivts_GroupSub'),
                DB::raw('MAX(ivts_Seat) AS ivts_Seat'),
                DB::raw('MAX(ivts_RsvpStatus) AS ivts_RsvpStatus'),
                DB::raw('MAX(ivts_Anggota) AS ivts_Anggota'),
                DB::raw("
                    CASE WHEN ivts_RsvpRespond = 0 THEN 'Tidak Hadir'
                        WHEN ivts_RsvpRespond = 1 THEN CONCAT('Hadir', '(', MAX(ivts_RsvpGuest), ')')
                        ELSE 'Belum Konfirmasi'
                    END AS ivts_RsvpRespond
                ")
            )
                ->where('ivts_Uuid', $req->qrCode);

            if (!empty($req->client_id)) {
                $data->where('ivts_Client_Id', $req->client_id);
            }

            $data->groupBy(
                'ivts_Client_Id',
                'ivts_Uuid',
                'ivts_Name',
                'ivts_Address'
            );

            if (!empty($data->first())) {
                return response()->json([
                    'status' => true,
                    'message' => 'Guest Invitation found !!',
                    'data' => $data->first()
                ], 200);
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'Guest Invitation not found !!',
                    'data' => $data->first()
                ], 200);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'label' => $th->getMessage(),
                'data' => [
                    'entity' => 'checkQRCode',
                    'action' => 'create',
                    'result' => $th->getMessage()
                ]
            ], 422);
        }
    }

    /**
     * @OA\Post(
     *     path="/ori/guestConfirmation",
     *     operationId="/ori/invitation/guestConfirmation",
     *     tags={"Invitations"},
     *     @OA\Parameter(
     *         name="client_id",
     *         in="query",
     *         description="Wedding client ID",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="qrCode",
     *         in="query",
     *         description="QR Code ID from client",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="guest_count",
     *         in="query",
     *         description="Guest count",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="souvenir_count",
     *         in="query",
     *         description="Souvenir get count",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="angpau_count",
     *         in="query",
     *         description="Angpau given count",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="gift_count",
     *         in="query",
     *         description="Gift given",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="operator",
     *         in="query",
     *         description="Operator logged in",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Return",
     *         @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="status", type="boolean"),
     *              @OA\Property(property="data", type="array", @OA\Items()),
     *              @OA\Property(property="message", type="string")
     *         )
     *     ),
     *     @OA\Response(
     *         response="422",
     *         description="Error: Some validation not passed",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items()
     *         )
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Error: username or password not right",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="status", type="boolean"),
     *              @OA\Property(property="message", type="string"),
     *         )
     *     ),
     * )
     */
    public function guestConfirmation(Request $req)
    {
        date_default_timezone_set('Asia/Jakarta');
        //validate incoming request
        $this->validate($req, [
            'qrCode' => 'required|string',
            'client_id' => 'required|string',
            // 'guest_count' => 'required|integer',
            // 'souvenir_count' => 'required|integer',
            // 'angpau_count' => 'required|integer',
            'operator' => 'required|string',
        ], [
            'qrCode.exists' => 'No QR Code found in database !'
        ]);

        try {
            $cekInvitation = Invitations::select(
                'ivts_Name',
                'ivts_Address',
                'ivts_NoHp',
                'ivts_Seat',
                'ivts_Group',
                'ivts_GroupFam',
                'ivts_GroupSub',
                'ivts_Category',
                DB::raw('COALESCE(SUM(ivts_GuestAtt),0) AS ivts_GuestAtt'),
                DB::raw('COALESCE(SUM(ivts_SouvenirAtt),0) AS ivts_SouvenirAtt'),
                DB::raw('COALESCE(SUM(ivts_AngpauAtt),0) AS ivts_AngpauAtt'),
                DB::raw('COALESCE(SUM(ivts_GiftAtt),0) AS ivts_GiftAtt')
            )
                ->where('ivts_Uuid', $req->qrCode)
                ->where(function ($f) {
                    $f->where('ivts_GuestAtt', '>', 0);
                    $f->orWhere('ivts_SouvenirAtt', '>', 0);
                    $f->orWhere('ivts_AngpauAtt', '>', 0);
                    // $f->orWhere('ivts_GiftAtt', '>', 0);
                })
                ->groupBy(
                    'ivts_Name',
                    'ivts_Address',
                    'ivts_NoHp',
                    'ivts_Seat',
                    'ivts_Group',
                    'ivts_GroupFam',
                    'ivts_GroupSub'
                )
                ->first();

            // return $cekInvitation;

            if (!empty($cekInvitation)) {
                $timenya = $req->has('timestamp') && !empty($req->timestamp) ? date('Y-m-d H:i:s', strtotime($req->timestamp)) :  date('Y-m-d H:i:s');
                $checkExistsInv = Invitations::where('ivts_Client_Id', $req->client_id)
                    ->where('ivts_Uuid', $req->qrCode)
                    ->where('ivts_GuestAttTime', $timenya)
                    // ->where('ivts_GuestAttTime', '>=', DB::raw('DATE_SUB(NOW(), INTERVAL 10 SECOND)'))
                    // ->where('ivts_GuestAttTime', '<=', DB::raw('NOW()'))
                    ->get()
                    ->toArray();

                if (count($checkExistsInv) > 0) {
                    return response()->json([
                        'status' => false,
                        'label' => 'Already registered !',
                        'data' => []
                    ], 200);
                }

                $hasil = $this->guestRegister(new Request([
                    'client_id' => $req->client_id,
                    'qrCode' => $req->qrCode,
                    'guest_name' => $cekInvitation->ivts_Name,
                    'guest_addr' => $cekInvitation->ivts_Address,
                    // 'guest_count_pred' => (int)$cekInvitation->ivts_Guest,
                    'guest_count' => $req->guest_count,
                    // 'souvenir_count_pred' => (int)$cekInvitation->ivts_Souvenir,
                    'souvenir_count' => $req->souvenir_count,
                    // 'angpau_count_pred' => (int)$cekInvitation->ivts_Angpau,
                    'angpau_count' => $req->angpau_count,
                    'gift_count' => $req->gift_count,
                    'gift' => $req->gift,
                    'operator' => $req->operator,
                    'no_hp' => $cekInvitation->ivts_NoHp,
                    'seat' => $cekInvitation->ivts_Seat,
                    'group' => $cekInvitation->ivts_Group,
                    'group_fam' => $cekInvitation->ivts_GroupFam,
                    'group_sub' => $cekInvitation->ivts_GroupSub,
                    'category' => $cekInvitation->ivts_Category,
                    'status' => 'Iterasi',
                    'timestamp' => $timenya,
                    'anggota' => $req->anggota,
                ]));
            } else {
                $hasil = Invitations::where('ivts_Uuid', $req->qrCode)
                    ->where('ivts_Client_Id', $req->client_id)
                    ->update([
                        'ivts_GuestAtt' => $req->guest_count,
                        'ivts_GuestAttTime' => $req->has('timestamp') && !empty($req->timestamp) ? date('Y-m-d H:i:s', strtotime($req->timestamp)) :  date('Y-m-d H:i:s'),
                        'ivts_GuestAttCounter' => $req->operator,
                        'ivts_SouvenirAtt' => $req->souvenir_count,
                        'ivts_SouvenirAttTime' => $req->has('timestamp') && !empty($req->timestamp) ? date('Y-m-d H:i:s', strtotime($req->timestamp)) :  date('Y-m-d H:i:s'),
                        'ivts_SouvenirAttCounter' => $req->operator,
                        'ivts_AngpauAtt' => $req->angpau_count,
                        'ivts_AngpauAttTime' => $req->has('timestamp') && !empty($req->timestamp) ? date('Y-m-d H:i:s', strtotime($req->timestamp)) :  date('Y-m-d H:i:s'),
                        'ivts_AngpauAttCounter' => $req->operator,
                        'ivts_GiftAtt' => $req->gift_count,
                        'ivts_GiftAttTime' => $req->has('timestamp') && !empty($req->timestamp) ? date('Y-m-d H:i:s', strtotime($req->timestamp)) :  date('Y-m-d H:i:s'),
                        'ivts_GiftAttCounter' => $req->operator,
                        'ivts_GiftDesc' => $req->gift,
                        'ivts_Anggota' => $req->anggota,
                        // 'ivts_RsvpStatus' => 'RSVP'
                    ]);
            }

            return response()->json([
                'status' => true,
                'label' => 'Invitation updated !',
                'data' => Invitations::where('ivts_Uuid', $req->qrCode)->get()->toArray(),
                'cek' => $hasil
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'label' => $th->getMessage(),
                'data' => [
                    'entity' => 'guestConfirmation',
                    'action' => 'create',
                    'result' => $th
                ]
            ], 422);
        }
    }

    /**
     * @OA\Post(
     *     path="/ori/guestRegister",
     *     operationId="/ori/invitation/guestRegister",
     *     tags={"Invitations"},
     *     @OA\Parameter(
     *         name="client_id",
     *         in="query",
     *         description="Wedding client ID",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="guest_name",
     *         in="query",
     *         description="Guest name",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="guest_addr",
     *         in="query",
     *         description="Guest Address / Instance",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="guest_count",
     *         in="query",
     *         description="Guest count",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(x
     *         name="souvenir_count",
     *         in="query",
     *         description="Souvenir get count",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="angpau_count",
     *         in="query",
     *         description="Angpau given count",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="gift_count",
     *         in="query",
     *         description="Gift given count",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="operator",
     *         in="query",
     *         description="Operator logged in",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Return",
     *         @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="status", type="boolean"),
     *              @OA\Property(property="data", type="array", @OA\Items()),
     *              @OA\Property(property="message", type="string")
     *         )
     *     ),
     *     @OA\Response(
     *         response="422",
     *         description="Error: there is an error on the server / some validation not work",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="status", type="boolean"),
     *              @OA\Property(property="message", type="string"),
     *         )
     *     ),
     * )
     */
    public function guestRegister(Request $req)
    {
        date_default_timezone_set('Asia/Jakarta');
        //validate incoming request
        $this->validate($req, [
            'client_id' => 'required|string',
            'guest_name' => 'required|string',
            // 'guest_addr' => 'required|string',
            // 'guest_count' => 'required|integer',
            // 'souvenir_count' => 'required|integer',
            // 'angpau_count' => 'required|integer',
            'operator' => 'required|string'
        ]);

        try {
            // return $req;
            $id = $this->checkID('');

            $data = Invitations::create([
                'ivts_Client_Id' => $req->client_id,
                'ivts_Uuid' => !$req->has('qrCode') || empty($req->qrCode) ? $id : $req->qrCode,
                'ivts_Name' => $req->guest_name,
                'ivts_Address' => $req->guest_addr,
                'ivts_NoHp' => $req->has('no_hp') && !empty($req->no_hp) ? $req->no_hp : '',
                'ivts_Seat' => $req->has('seat') && !empty($req->seat) ? $req->seat : '',
                'ivts_Group' => $req->has('group') && !empty($req->group) ? $req->group : '',
                'ivts_GroupFam' => $req->has('group_fam') && !empty($req->group_fam) ? $req->group_fam : '',
                'ivts_GroupSub' => $req->has('group_sub') && !empty($req->group_sub) ? $req->group_sub : '',
                'ivts_Guest' => (int)$req->guest_count_pred,
                'ivts_GuestAtt' => $req->guest_count,
                'ivts_GuestAttTime' => $req->timestamp,
                'ivts_GuestAttCounter' => $req->operator,
                'ivts_Souvenir' => (int)$req->souvenir_count_pred,
                'ivts_SouvenirAtt' => $req->souvenir_count,
                'ivts_SouvenirAttTime' => $req->timestamp,
                'ivts_SouvenirAttCounter' => $req->operator,
                'ivts_Angpau' => (int)$req->angpau_count_pred,
                'ivts_AngpauAtt' => $req->angpau_count,
                'ivts_AngpauAttTime' => $req->timestamp,
                'ivts_AngpauAttCounter' => $req->operator,
                'ivts_Gift' => (int)$req->gift_count_pred,
                'ivts_GiftDesc' => $req->gift,
                'ivts_GiftAtt' => $req->gift_count,
                'ivts_GiftAttTime' => $req->timestamp,
                'ivts_GiftAttCounter' => $req->operator,
                'ivts_RsvpStatus' => !$req->has('status') || empty($req->status) ? 'Input Manual' : $req->status,
                'ivts_RsvpRespond' => !$req->has('respond') || empty($req->respond)
                    ? 3
                    : $req->respond,
                // 'ivts_Category' => !$req->has('category') || empty($req->category) ? 'Input Manual' : $req->category,
                'ivts_Category' => $req->category,
            ]);

            return response()->json([
                'status' => true,
                'label' => 'Guest added yaa!',
                'data' => $data
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'label' => $th->getMessage(),
                'data' => [
                    'entity' => 'guestRegister',
                    'action' => 'create',
                    'result' => $th->getMessage()
                ]
            ], 422);
        }
    }

    public function checkID($id)
    {
        if (empty($id)) {
            $ids = $this->generateRandomString(8) . '-' . $this->generateRandomString(4) . '-' . $this->generateRandomString(4) . '-' . $this->generateRandomString(4) . '-' . $this->generateRandomString(12);

            $cekData = Invitations::where('ivts_Uuid', $ids)->first();

            if (empty($cekData)) {
                return $ids;
            } else {
                return $this->checkID('');
            }
        } else {
            $cekData = Invitations::where('ivts_Uuid', $id)->first();
            if (empty($cekData)) {
                return $id;
            } else {
                return $this->checkID('');
            }
        }
    }

    public function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * @OA\Post(
     *     path="/ori/searchGuest",
     *     operationId="/ori/invitation/searchGuest",
     *     tags={"Invitations"},
     *     @OA\Parameter(
     *         name="client_id",
     *         in="query",
     *         description="Wedding client ID",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="query_col",
     *         in="query",
     *         description="DB Column that will be searched",
     *         required=true,
     *         @OA\Schema
     *         (
     *           type="array",
     *           @OA\Items(
     *               type="string",
     *               description="Invitations table columns"
     *           )
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="query_op",
     *         in="query",
     *         description="Operation of data that will be searched, '=' for exacly value search or 'like' for contains value search",
     *         required=true,
     *         @OA\Schema
     *         (
     *           type="array",
     *           @OA\Items(
     *               type="string",
     *               description="Operation list"
     *           )
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="query_val",
     *         in="query",
     *         description="Value of columns that will be searched",
     *         required=true,
     *         @OA\Schema
     *         (
     *           type="array",
     *           @OA\Items(
     *               type="string",
     *               description="Value List"
     *           )
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Return",
     *         @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="status", type="boolean"),
     *              @OA\Property(property="data", type="array", @OA\Items()),
     *              @OA\Property(property="message", type="string")
     *         )
     *     ),
     *     @OA\Response(
     *         response="422",
     *         description="Error: there is an error on the server / backend",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="status", type="boolean"),
     *              @OA\Property(property="message", type="string"),
     *         )
     *     ),
     * )
     */
    public function searchGuest(Request $req)
    {
        //validate incoming request
        $this->validate($req, [
            'client_id' => 'required|string',
            // 'query_search' => 'required|string',
            'query_col' => 'required|array|min:1',
            'query_col.*' => 'required|string|distinct|min:1',
            'query_op' => 'required|array|min:1',
            'query_op.*' => 'required|string|min:1',
            'query_val' => 'required|array|min:1',
            'query_val.*' => 'required|string|distinct|min:1'
        ]);

        try {
            $dataHeader = DB::connection('mysql_db_old')->table('invitations as inv2')->select(
                'ivts_Client_Id',
                'ivts_Uuid',
                'ivts_Name',
                'ivts_Address',
                DB::raw("COUNT(*) + (
                    SELECT COUNT(*)
                    FROM invitations inv
                    WHERE inv.ivts_RsvpStatus = 'Iterasi'
                    and inv.ivts_Client_Id = inv2.ivts_Client_Id
                    and inv.ivts_Uuid = inv2.ivts_Uuid
                ) AS ivts_TotalAtt"),
                DB::raw('MAX(ivts_NoHp) AS ivts_NoHp'),
                DB::raw('MAX(ivts_Guest) AS ivts_Guest'),
                // DB::raw("COALESCE(SUM(ivts_GuestAtt) + (
                //     SELECT COALESCE(SUM(ivts_GuestAtt),0)
                //     FROM invitations inv
                //     WHERE inv.ivts_RsvpStatus = 'Iterasi'
                //     and inv.ivts_Client_Id = inv2.ivts_Client_Id
                //     and inv.ivts_Uuid = inv2.ivts_Uuid
                // ),0) AS ivts_GuestAtt"),
                DB::raw("COALESCE(SUM(ivts_GuestAtt) + (
                    SELECT COALESCE(SUM(ivts_GuestAtt),0)
                    FROM invitations inv
                    WHERE inv.ivts_RsvpStatus = 'Iterasi'
                    and inv.ivts_Client_Id = inv2.ivts_Client_Id
                    and inv.ivts_Uuid = inv2.ivts_Uuid
                ),0) AS ivts_GuestAtt"),
                DB::raw('MAX(ivts_GuestAttTime) AS ivts_GuestAttTime'),
                DB::raw('MAX(ivts_GuestAttCounter) AS ivts_GuestAttCounter'),
                DB::raw('MAX(ivts_Souvenir) AS ivts_Souvenir'),
                DB::raw("COALESCE(SUM(ivts_SouvenirAtt) + (
                    SELECT COALESCE(SUM(ivts_SouvenirAtt),0)
                    FROM invitations inv
                    WHERE inv.ivts_RsvpStatus = 'Iterasi'
                    and inv.ivts_Client_Id = inv2.ivts_Client_Id
                    and inv.ivts_Uuid = inv2.ivts_Uuid
                ),0) AS ivts_SouvenirAtt"),
                DB::raw('MAX(ivts_SouvenirAttTime) AS ivts_SouvenirAttTime'),
                DB::raw('MAX(ivts_SouvenirAttCounter) AS ivts_SouvenirAttCounter'),
                DB::raw('MAX(ivts_Angpau) AS ivts_Angpau'),
                DB::raw("COALESCE(SUM(ivts_AngpauAtt) + (
                    SELECT COALESCE(SUM(ivts_AngpauAtt),0)
                    FROM invitations inv
                    WHERE inv.ivts_RsvpStatus = 'Iterasi'
                    and inv.ivts_Client_Id = inv2.ivts_Client_Id
                    and inv.ivts_Uuid = inv2.ivts_Uuid
                ),0) AS ivts_AngpauAtt"),
                DB::raw('MAX(ivts_AngpauAttTime) AS ivts_AngpauAttTime'),
                DB::raw('MAX(ivts_AngpauAttCounter) AS ivts_AngpauAttCounter'),
                DB::raw('MAX(ivts_Gift) AS ivts_GiftOld'),
                DB::raw("COALESCE(SUM(ivts_GiftAtt) + (
                    SELECT COALESCE(SUM(ivts_GiftAtt),0)
                    FROM invitations inv
                    WHERE inv.ivts_RsvpStatus = 'Iterasi'
                    and inv.ivts_Client_Id = inv2.ivts_Client_Id
                    and inv.ivts_Uuid = inv2.ivts_Uuid
                ),0) AS ivts_GiftAtt"),
                DB::raw('MAX(ivts_GiftAttTime) AS ivts_GiftAttTime'),
                DB::raw('MAX(ivts_GiftAttCounter) AS ivts_GiftAttCounter'),
                DB::raw('MAX(ivts_GiftDesc) AS ivts_Gift'),
                DB::raw('MAX(ivts_Category) AS ivts_Category'),
                DB::raw('MAX(ivts_Group) AS ivts_Group'),
                DB::raw('MAX(ivts_GroupFam) AS ivts_GroupFam'),
                DB::raw('MAX(ivts_GroupSub) AS ivts_GroupSub'),
                DB::raw('MAX(ivts_Seat) AS ivts_Seat'),
                DB::raw('MAX(ivts_RsvpStatus) AS ivts_RsvpStatus'),
                DB::raw('MAX(ivts_Anggota) AS ivts_Anggota'),
                DB::raw("
                    CASE WHEN MAX(ivts_RsvpRespond) = 0 THEN 'Tidak Hadir'
                        WHEN MAX(ivts_RsvpRespond) = 1 THEN CONCAT('Hadir', '(', MAX(ivts_RsvpGuest), ')')
                        ELSE 'Belum Konfirmasi'
                    END AS ivts_RsvpRespond
                ")
            )
                ->where('ivts_Client_Id', $req->client_id);

            if ($req->has('query_col') && !empty($req->query_col)) {
                foreach ($req->query_col as $key => $valCol) {
                    $dataHeader->where(
                        'inv2.' . $valCol,
                        $req->query_op[$key],
                        $req->query_op[$key] === 'like'
                            ? '%' . $req->query_val[$key] . '%'
                            : $req->query_val[$key]
                    );
                }
            }

            // $headerName = is_numeric($req->query_search) === true
            //     ? $dataHeader->where('ivts_NoHp', 'like', '%' . $req->query_search . '%')
            //     : $dataHeader->where('ivts_Name', 'like', '%' . $req->query_search . '%');

            $data = $dataHeader
                ->whereIn('ivts_RsvpStatus', ['RSVP', 'Undangan'])
                ->groupBy(
                    'ivts_Client_Id',
                    'ivts_Uuid',
                    'ivts_Name',
                    'ivts_NoHp',
                    'ivts_Address'
                );

            // return $data;
            if (count($data->get()) > 0) {
                return response()->json([
                    'status' => true,
                    'label' => 'Search found !',
                    'data' => $data->get()
                ], 200);
            } else {
                return response()->json([
                    'status' => false,
                    'label' => 'Search not found !',
                    'data' => $data->get(),
                    'test' => is_int($req->query_search),
                    'test2' => $data->toSql()
                ], 422);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'label' => $th->getMessage(),
                'data' => [
                    'entity' => 'searchGuest',
                    'action' => 'create',
                    'result' => $th->getMessage()
                ]
            ], 422);
        }
    }

    /**
     * @OA\Post(
     *     path="/ori/dashboardCard",
     *     operationId="/ori/invitation/dashboardCard",
     *     tags={"Invitations"},
     *     @OA\Parameter(
     *         name="client_id",
     *         in="query",
     *         description="Wedding client ID",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Return",
     *         @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="status", type="boolean"),
     *              @OA\Property(property="data", type="array", @OA\Items()),
     *              @OA\Property(property="message", type="string")
     *         )
     *     ),
     *     @OA\Response(
     *         response="422",
     *         description="Error: there is an error on the server / backend",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="status", type="boolean"),
     *              @OA\Property(property="message", type="string"),
     *         )
     *     ),
     * )
     */
    public function dashboardCard(Request $req, $datatable = [])
    {
        //validate incoming request
        $this->validate($req, [
            'client_id' => 'required|string'
        ]);

        try {
            $dataRSVP = DB::connection('mysql_db_old')->table('invitations as inv2')->where('ivts_Client_Id', $req->client_id);
            $dataRSVPIterasi = DB::connection('mysql_db_old')->table('invitations as inv2')->where('ivts_Client_Id', $req->client_id);
            $dataManual = DB::connection('mysql_db_old')->table('invitations as inv2')->where('ivts_Client_Id', $req->client_id);
            $dataTotal = DB::connection('mysql_db_old')->table('invitations as inv2')->where('ivts_Client_Id', $req->client_id);
            $dataTotalIterasi = DB::connection('mysql_db_old')->table('invitations as inv2')->where('ivts_Client_Id', $req->client_id);

            if ($req->has('qrCode') && !empty($req->qrCode)) {
                $dataRSVP->where('ivts_Uuid', $req->qrCode);
                $dataRSVPIterasi->where('ivts_Uuid', $req->qrCode);
                $dataManual->where('ivts_Uuid', $req->qrCode);
                $dataTotal->where('ivts_Uuid', $req->qrCode);
                $dataTotalIterasi->where('ivts_Uuid', $req->qrCode);
            }

            $dataRSVP->whereIn('ivts_RsvpStatus', ['RSVP', 'Undangan']);
            $dataRSVPIterasi->whereIn('ivts_RsvpStatus', ['RSVP', 'Undangan', 'Iterasi']);
            $dataManual->whereIn('ivts_RsvpStatus', ['Input Manual']);
            $dataTotal->whereIn('ivts_RsvpStatus', ['RSVP', 'Undangan', 'Input Manual']);
            $dataTotalIterasi->whereIn('ivts_RsvpStatus', ['RSVP', 'Undangan', 'Input Manual', 'Iterasi']);

            $dataRSVP = $this->dynamicFilter($dataRSVP, $req->column, 'inv2.');
            $dataRSVPIterasi = $this->dynamicFilter($dataRSVPIterasi, $req->column, 'inv2.');
            $dataManual = $this->dynamicFilter($dataManual, $req->column, 'inv2.');
            $dataTotal = $this->dynamicFilter($dataTotal, $req->column, 'inv2.');
            $dataTotalIterasi = $this->dynamicFilter($dataTotalIterasi, $req->column, 'inv2.');
            // if ($req->has('column') && !empty($req->column['query_col'])) {
            //     foreach ($req->column['query_col'] as $key => $valCol) {
            //         if ($valCol === 'ivts_RsvpRespond') {
            //             $status = [
            //                 [
            //                     'desc' => 'HADIR',
            //                     'flag' => 1
            //                 ],
            //                 [
            //                     'desc' => 'Tidak Hadir',
            //                     'flag' => 0
            //                 ],
            //                 [
            //                     'desc' => 'Iterasi / Manual',
            //                     'flag' => 3
            //                 ],
            //                 [
            //                     'desc' => 'Belum Konfirmasi',
            //                     'flag' => 2
            //                 ],
            //                 [
            //                     'desc' => 'Belum Konfirmasi',
            //                     'flag' => 102
            //                 ],
            //                 [
            //                     'desc' => 'Belum Konfirmasi',
            //                     'flag' => 'NULL'
            //                 ],
            //             ];

            //             $cekDataFilter = array_values(
            //                 array_filter($status, function ($f) use ($req, $key) {
            //                     return strpos($f['desc'], $req->column['query_val'][$key]) !== false;
            //                 })
            //             );

            //             $getValue = [];
            //             foreach ($cekDataFilter as $keyVal => $valueVal) {
            //                 $getValue = $valueVal['flag'];
            //             }

            //             $valuenya = $getValue;
            //         } else {
            //             $valuenya = $req->column['query_val'][$key];
            //         }

            //         if (is_array($valuenya)) {
            //             $dataRSVP->whereIn(
            //                 'inv2.' . $valCol,
            //                 $valuenya
            //             );

            //             $dataRSVPIterasi->whereIn(
            //                 'inv2.' . $valCol,
            //                 $valuenya
            //             );

            //             $dataManual->whereIn(
            //                 'inv2.' . $valCol,
            //                 $valuenya
            //             );

            //             $dataTotal->whereIn(
            //                 'inv2.' . $valCol,
            //                 $valuenya
            //             );
            //         } else {
            //             $dataRSVP->where(
            //                 'inv2.' . $valCol,
            //                 $req->column['query_op'][$key],
            //                 $req->column['query_op'][$key] === 'like'
            //                     ? '%' . $valuenya . '%'
            //                     : $valuenya
            //             );

            //             $dataRSVPIterasi->where(
            //                 'inv2.' . $valCol,
            //                 $req->column['query_op'][$key],
            //                 $req->column['query_op'][$key] === 'like'
            //                     ? '%' . $valuenya . '%'
            //                     : $valuenya
            //             );

            //             $dataManual->where(
            //                 'inv2.' . $valCol,
            //                 $req->column['query_op'][$key],
            //                 $req->column['query_op'][$key] === 'like'
            //                     ? '%' . $valuenya . '%'
            //                     : $valuenya
            //             );

            //             $dataTotal->where(
            //                 'inv2.' . $valCol,
            //                 $req->column['query_op'][$key],
            //                 $req->column['query_op'][$key] === 'like'
            //                     ? '%' . $valuenya . '%'
            //                     : $valuenya
            //             );
            //         }
            //     }
            // }

            $dataRSVP->leftJoin(DB::raw("(
                SELECT
                    inv.ivts_Uuid,
                    COALESCE(SUM(ivts_GuestAtt),0) as ivts_GuestAttIter,
                    COALESCE(SUM(ivts_SouvenirAtt),0) as ivts_SouvenirAttIter,
                    COALESCE(SUM(ivts_AngpauAtt),0) as ivts_AngpauAttIter,
                    COALESCE(SUM(ivts_GiftAtt),0) as ivts_GiftAttIter
                FROM invitations inv
                WHERE inv.ivts_RsvpStatus = 'Iterasi'
                group by inv.ivts_Uuid
            ) inv"), function ($j) {
                $j->on('inv.ivts_Uuid', 'inv2.ivts_Uuid');
            });

            $dataManual->leftJoin(DB::raw("(
                SELECT
                    inv.ivts_Uuid,
                    COALESCE(SUM(ivts_GuestAtt),0) as ivts_GuestAttIter,
                    COALESCE(SUM(ivts_SouvenirAtt),0) as ivts_SouvenirAttIter,
                    COALESCE(SUM(ivts_AngpauAtt),0) as ivts_AngpauAttIter,
                    COALESCE(SUM(ivts_GiftAtt),0) as ivts_GiftAttIter
                FROM invitations inv
                WHERE inv.ivts_RsvpStatus = 'Iterasi'
                group by inv.ivts_Uuid
            ) inv"), function ($j) {
                $j->on('inv.ivts_Uuid', 'inv2.ivts_Uuid');
            });

            $dataTotal->leftJoin(DB::raw("(
                SELECT
                    inv.ivts_Uuid,
                    COALESCE(SUM(ivts_GuestAtt),0) as ivts_GuestAttIter,
                    COALESCE(SUM(ivts_SouvenirAtt),0) as ivts_SouvenirAttIter,
                    COALESCE(SUM(ivts_AngpauAtt),0) as ivts_AngpauAttIter,
                    COALESCE(SUM(ivts_GiftAtt),0) as ivts_GiftAttIter
                FROM invitations inv
                WHERE inv.ivts_RsvpStatus = 'Iterasi'
                group by inv.ivts_Uuid
            ) inv"), function ($j) {
                $j->on('inv.ivts_Uuid', 'inv2.ivts_Uuid');
            });

            $hasil = [
                'RSVP' => [
                    'TOTAL' => (clone $dataRSVP)->count(),
                    'HADIR' => [
                        'ORANG' => (int)(clone $dataRSVP)->where('ivts_RsvpRespond', 1)->select(DB::raw('COALESCE(SUM(ivts_RsvpGuest),0) as tot'))->first()->tot,
                        'UNDANGAN' => (clone $dataRSVP)->where('ivts_RsvpRespond', 1)->count()
                    ],
                    'TIDAK_HADIR' => [
                        'CONFIRM_RSVP' => (clone $dataRSVP)->select(DB::raw('COALESCE(SUM(ivts_GuestAtt),0) as tot'))->where('ivts_RsvpRespond', 0)->where('ivts_GuestAtt', '>', 0)->count(),
                        'TOTAL' => (clone $dataRSVP)->where('ivts_RsvpRespond', 0)->count()
                    ],
                    'UNCONFIRM' => [
                        'CONFIRM_RSVP' => (clone $dataRSVP)->select(DB::raw('COALESCE(SUM(ivts_GuestAtt),0) as tot'))->where(function ($w) {
                            $w->whereNotIn('ivts_RsvpRespond', [1, 0, 3]);
                            $w->orWhere('ivts_RsvpRespond', NULL);
                        })->where('ivts_GuestAtt', '>', 0)->count(),
                        'TOTAL' => (clone $dataRSVP)->where(function ($w) {
                            $w->whereNotIn('ivts_RsvpRespond', [1, 0, 3]);
                            $w->orWhere('ivts_RsvpRespond', NULL);
                        })->count()
                    ]
                ],
                'UNDANGAN' => [
                    'HADIR' => [
                        'CONFIRM_RSVP' => (clone $dataRSVP)->select(DB::raw('COALESCE(SUM(ivts_GuestAtt),0) as tot'))->where(function ($f) {
                            $f->where('ivts_GuestAtt', '>', 0);
                            $f->orWhere('ivts_SouvenirAtt', '>', 0);
                            $f->orWhere('ivts_AngpauAtt', '>', 0);
                            // $f->orWhere('ivts_GiftAtt', '>', 0);
                        })->count(),
                        'PRED_RSVP' => (int)(clone $dataRSVP)->count()
                    ],
                    'GUEST' => [
                        'CONFIRM_RSVP' => (clone $dataRSVPIterasi)->select(
                            DB::raw("
                                COALESCE(SUM(ivts_GuestAtt),0) as tot
                            ")
                        )
                            ->where(function ($f) {
                                $f->where('ivts_GuestAtt', '>', 0);
                                $f->orWhere('ivts_SouvenirAtt', '>', 0);
                                $f->orWhere('ivts_AngpauAtt', '>', 0);
                                // $f->orWhere('ivts_GiftAtt', '>', 0);
                            })
                            ->first()
                            ->tot,
                        'PRED_RSVP' => (int)(clone $dataRSVPIterasi)->select(DB::raw('SUM(ivts_Guest) as tot'))->first()->tot
                    ],
                    'SOUVENIR' => [
                        'CONFIRM_RSVP' => (int)(clone $dataRSVPIterasi)->select(
                            DB::raw("
                                COALESCE(SUM(ivts_SouvenirAtt),0) as tot
                            ")
                        )
                            // ->where('ivts_SouvenirAtt', '>', 0)
                            ->first()
                            ->tot,
                        'PRED_RSVP' => (int)(clone $dataRSVPIterasi)->select(DB::raw('SUM(ivts_Souvenir) as tot'))->first()->tot,
                    ],
                    'ANGPAU' => [
                        'CONFIRM_RSVP' => (int)(clone $dataRSVPIterasi)->select(
                            DB::raw("
                                COALESCE(SUM(ivts_AngpauAtt),0) as tot
                            ")
                        )
                            // ->where('ivts_AngpauAtt', '>', 0)
                            ->first()
                            ->tot,
                        'PRED_RSVP' => (int)(clone $dataRSVPIterasi)->select(DB::raw('SUM(ivts_Angpau) as tot'))->first()->tot,
                    ],
                    'GIFT' => [
                        'CONFIRM_RSVP' => (int)(clone $dataRSVPIterasi)->select(
                            DB::raw("
                                COALESCE(SUM(ivts_GiftAtt),0) as tot
                            ")
                        )
                            // ->where('ivts_AngpauAtt', '>', 0)
                            ->first()
                            ->tot,
                        'PRED_RSVP' => (int)(clone $dataRSVPIterasi)->select(DB::raw('SUM(ivts_Gift) as tot'))->first()->tot,
                    ]
                ],
                'MANUAL' => [
                    'HADIR' => [
                        'CONFIRM_RSVP' => (clone $dataManual)->select(DB::raw('COALESCE(SUM(ivts_GuestAtt),0) as tot'))->where(function ($f) {
                            $f->where('ivts_GuestAtt', '>', 0);
                            $f->orWhere('ivts_SouvenirAtt', '>', 0);
                            $f->orWhere('ivts_AngpauAtt', '>', 0);
                            // $f->orWhere('ivts_GiftAtt', '>', 0);
                        })->count(),
                        'PRED_RSVP' => (clone $dataManual)->count()
                    ],
                    'GUEST' => [
                        'CONFIRM_RSVP' => (int)(clone $dataManual)->select(
                            DB::raw("
                                COALESCE(SUM(ivts_GuestAtt),0) +
                                COALESCE(SUM(inv.ivts_GuestAttIter), 0) as tot
                            ")
                        )
                            ->where(function ($f) {
                                $f->where('ivts_GuestAtt', '>', 0);
                                $f->orWhere('ivts_SouvenirAtt', '>', 0);
                                $f->orWhere('ivts_AngpauAtt', '>', 0);
                                // $f->orWhere('ivts_GiftAtt', '>', 0);
                            })
                            ->first()
                            ->tot,
                        'PRED_RSVP' => (int)(clone $dataManual)->select(DB::raw('SUM(ivts_Guest) as tot'))->first()->tot,
                    ],
                    'SOUVENIR' => [
                        'CONFIRM_RSVP' => (int)(clone $dataManual)->select(
                            DB::raw("
                                COALESCE(SUM(ivts_SouvenirAtt),0) +
                                COALESCE(SUM(ivts_SouvenirAttIter),0) as tot
                            ")
                        )
                            // ->where('ivts_SouvenirAtt', '>', 0)
                            ->first()
                            ->tot,
                        'PRED_RSVP' => (int)(clone $dataManual)->select(DB::raw('SUM(ivts_Souvenir) as tot'))->first()->tot,
                    ],
                    'ANGPAU' => [
                        'CONFIRM_RSVP' => (int)(clone $dataManual)->select(
                            DB::raw("
                                COALESCE(SUM(ivts_AngpauAtt),0) +
                                COALESCE(SUM(ivts_AngpauAttIter),0) as tot
                            ")
                        )
                            // ->where('ivts_AngpauAtt', '>', 0)
                            ->first()
                            ->tot,
                        'PRED_RSVP' => (int)(clone $dataManual)->select(DB::raw('SUM(ivts_Angpau) as tot'))->first()->tot,
                    ],
                    'GIFT' => [
                        'CONFIRM_RSVP' => (int)(clone $dataManual)->select(
                            DB::raw("
                                COALESCE(SUM(ivts_GiftAtt),0) +
                                COALESCE(SUM(ivts_GiftAttIter),0) as tot
                            ")
                        )
                            // ->where('ivts_AngpauAtt', '>', 0)
                            ->first()
                            ->tot,
                        'PRED_RSVP' => (int)(clone $dataManual)->select(DB::raw('SUM(ivts_Gift) as tot'))->first()->tot,
                    ]
                ],
                'TOTAL' => [
                    'HADIR' => [
                        'CONFIRM_RSVP' => (clone $dataTotal)->select(DB::raw('COALESCE(SUM(ivts_GuestAtt),0) as tot'))->where(function ($f) {
                            $f->where('ivts_GuestAtt', '>', 0);
                            $f->orWhere('ivts_SouvenirAtt', '>', 0);
                            $f->orWhere('ivts_AngpauAtt', '>', 0);
                            // $f->orWhere('ivts_GiftAtt', '>', 0);
                        })->count(),
                        'PRED_RSVP' => (clone $dataTotal)->count()
                    ],
                    'GUEST' => [
                        'CONFIRM_RSVP' => (int)(clone $dataTotalIterasi)->select(
                            DB::raw("
                                COALESCE(SUM(ivts_GuestAtt),0) as tot
                            ")
                        )
                            ->where(function ($f) {
                                $f->where('ivts_GuestAtt', '>', 0);
                                $f->orWhere('ivts_SouvenirAtt', '>', 0);
                                $f->orWhere('ivts_AngpauAtt', '>', 0);
                                // $f->orWhere('ivts_GiftAtt', '>', 0);
                            })
                            ->first()
                            ->tot,
                        'PRED_RSVP' => (int)(clone $dataTotalIterasi)->select(DB::raw('SUM(ivts_Guest) as tot'))->first()->tot,
                    ],
                    'SOUVENIR' => [
                        'CONFIRM_RSVP' => (int)(clone $dataTotalIterasi)->select(
                            DB::raw("
                                COALESCE(SUM(ivts_SouvenirAtt),0) as tot
                            ")
                        )
                            // ->where('ivts_SouvenirAtt', '>', 0)
                            ->first()
                            ->tot,
                        'PRED_RSVP' => (int)(clone $dataTotalIterasi)->select(DB::raw('SUM(ivts_Souvenir) as tot'))->first()->tot,
                    ],
                    'ANGPAU' => [
                        'CONFIRM_RSVP' => (int)(clone $dataTotalIterasi)->select(
                            DB::raw("
                                COALESCE(SUM(ivts_AngpauAtt),0) as tot
                            ")
                        )
                            // ->where('ivts_AngpauAtt', '>', 0)
                            ->first()
                            ->tot,
                        'PRED_RSVP' => (int)(clone $dataTotalIterasi)->select(DB::raw('SUM(ivts_Angpau) as tot'))->first()->tot,
                    ],
                    'GIFT' => [
                        'CONFIRM_RSVP' => (int)(clone $dataTotalIterasi)->select(
                            DB::raw("
                                COALESCE(SUM(ivts_GiftAtt),0) as tot
                            ")
                        )
                            // ->where('ivts_AngpauAtt', '>', 0)
                            ->first()
                            ->tot,
                        'PRED_RSVP' => (int)(clone $dataTotalIterasi)->select(DB::raw('SUM(ivts_Gift) as tot'))->first()->tot,
                    ]
                ]
            ];

            return response()->json([
                'status' => true,
                'label' => 'Dashboard found',
                'data' => $hasil
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'label' => $th->getMessage(),
                'data' => [
                    'entity' => 'dashboardCard',
                    'action' => 'create',
                    'result' => $th->getMessage()
                ]
            ], 422);
        }
    }

    /**
     * @OA\Post(
     *     path="/ori/datatableIvts",
     *     operationId="/ori/invitation/datatableIvts",
     *     tags={"Invitations"},
     *     @OA\Parameter(
     *         name="client_id",
     *         in="query",
     *         description="Wedding client ID",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="is_iterasi",
     *         in="query",
     *         description="Iterasi fetch data only",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="qrCode",
     *         in="query",
     *         description="QR Code guest",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="pagination",
     *         in="query",
     *         description="Pagination list",
     *         required=false,
     *         @OA\Schema
     *         (
     *           type="object",
     *           @OA\Property(
     *               property="pagination",
     *               type="object",
     *               @OA\Property(property="rowsPerPage", type="integer"),
     *               @OA\Property(property="page", type="integer")
     *           )
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="column",
     *         in="query",
     *         description="Column by filter",
     *         required=false,
     *         @OA\Schema
     *         (
     *             type="object",
     *             @OA\Property(
     *              property="column",
     *              type="object",
     *              @OA\Property(
     *                 property="query_col",
     *                 description="DB Column that will be searched",
     *                 type="array",
     *                 @OA\Items()
     *              ),
     *              @OA\Property(
     *                 property="query_op",
     *                 description="Operation of data that will be searched, '=' for exacly value search or 'like' for contains value search",
     *                 type="array",
     *                 @OA\Items()
     *              ),
     *              @OA\Property(
     *                 property="query_val",
     *                 description="Value of columns that will be searched",
     *                 type="array",
     *                 @OA\Items()
     *              ),
     *            )
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Return",
     *         @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="status", type="boolean"),
     *              @OA\Property(property="data", type="array", @OA\Items()),
     *              @OA\Property(property="message", type="string")
     *         )
     *     ),
     *     @OA\Response(
     *         response="422",
     *         description="Error: there is an error on the server / backend",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="status", type="boolean"),
     *              @OA\Property(property="message", type="string"),
     *         )
     *     ),
     * )
     */
    public function datatableIvts(Request $req)
    {
        $this->validate($req, [
            'client_id' => 'required|string'
            // 'pagination.rowsPerPage' => 'required|integer',
            // 'pagination.page' => 'required|integer'
        ]);

        try {
            // return 'masuk sini';
            $data = $this->getIvts(
                $req->client_id,
                '',
                $req->has('column') ? $req->column : [],
                ['Undangan', 'RSVP', 'Input Manual']
            );

            // return $data->toSql();

            if ($req->has('pagination')) {
                $rows = (int)$req['pagination']['rowsPerPage'] > 0
                    ? $req['pagination']['rowsPerPage']
                    : count($data->get()->toArray());

                $page = (int)$req['pagination']['page'] > 0
                    ? $req['pagination']['page']
                    : 1;

                if (!$req->has('is_iterasi') && $req->is_iterasi != 1) {
                    $combineIterasi = $this->viewIterasiList(
                        $data->get()->toArray(),
                        $req->has('column') && !empty($req->column)
                            ? $req->column
                            : null,
                        $req->has('filter') && !empty($req->filter)
                            ? $req->filter
                            : null
                    );

                    $convertData = collect($combineIterasi);

                    return response()->json([
                        'status' => true,
                        'label' => 'Data found !',
                        'data' => [
                            'table' => $convertData->paginate($rows, $page),
                            'dashboards' => $this->dashboardCard(new Request($req->all()))->original['data']
                        ]
                    ], 200);
                }

                return response()->json([
                    'status' => true,
                    'label' => 'Data found !',
                    'data' => [
                        'table' => $data->paginate($rows, ['*'], 'page', $page),
                        'dashboards' => $this->dashboardCard(new Request($req->all()))->original['data']
                    ]
                ], 200);
            } else {
                if (!$req->has('is_iterasi') && $req->is_iterasi != 1) {
                    $combineIterasi = $this->viewIterasiList(
                        $data->get()->toArray(),
                        $req->has('column') && !empty($req->column)
                            ? $req->column
                            : null,
                        $req->has('filter') && !empty($req->filter)
                            ? $req->filter
                            : null
                    );

                    return response()->json([
                        'status' => true,
                        'label' => 'Data found !',
                        'data' => [
                            'table' => $combineIterasi,
                            'dashboards' => $this->dashboardCard(new Request($req->all()), $data->get()->toArray())->original['data']
                        ]
                    ], 200);
                }

                return response()->json([
                    'status' => true,
                    'label' => 'Data found !',
                    'data' => [
                        'table' => $data->get(),
                        'dashboards' => $this->dashboardCard(new Request($req->all()), $data->get()->toArray())->original['data']
                    ]
                ], 200);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'label' => $th->getMessage(),
                'data' => [
                    'entity' => 'datatableIvts',
                    'action' => 'create',
                    'result' => $th->getMessage()
                ]
            ], 422);
        }
    }

    public function viewIterasiList($data, $cols = null, $filter = null)
    {
        $joinIterasi = [];

        $data = array_map(function ($value) {
            return (array)$value;
        }, $data);

        foreach ($data as $key => $value) {
            $cekUserDoubel = array_values(array_filter($data, function ($f) use ($value) {
                return $f['ivts_Uuid'] === $value['ivts_Uuid'];
            }));

            $inv = Invitations::where('ivts_Client_Id', $value['ivts_Client_Id'])
                ->where('ivts_Uuid', $value['ivts_Uuid']);

            if (!empty($cols)) {
                foreach ($cols['query_col'] as $key => $valCol) {
                    $inv->where(
                        $valCol,
                        $cols['query_op'][$key],
                        $cols['query_op'][$key] === 'like'
                            ? '%' . $cols['query_val'][$key] . '%'
                            : $cols['query_val'][$key]
                    );
                }
            }

            $dataIterasi = $inv->orderBy('ivts_GuestAttTime', 'asc')
                ->get()
                ->toArray();

            $getTotalGuest = $getTotalSouvenir = $getTotalAngpau = $getTotalGift = 0;

            foreach ($dataIterasi as $keyIter => $valueIterCal) {
                $getTotalGuest += $valueIterCal['ivts_GuestAtt'];
                $getTotalSouvenir += $valueIterCal['ivts_SouvenirAtt'];
                $getTotalAngpau += $valueIterCal['ivts_AngpauAtt'];
                $getTotalGift += $valueIterCal['ivts_GiftAtt'];
            }

            $value = array_merge($value, [
                'ivts_GuestAtt' => $getTotalGuest,
                'ivts_SouvenirAtt' => $getTotalSouvenir,
                'ivts_AngpauAtt' => $getTotalAngpau,
                'ivts_GiftAtt' => $getTotalGift,
                'ivts_GuestAttTime' => count($dataIterasi) > 0 ? $dataIterasi[count($dataIterasi) - 1]['ivts_GuestAttTime'] : '',
                'ivts_GuestAttCounter' => count($dataIterasi) > 0 ? $dataIterasi[count($dataIterasi) - 1]['ivts_GuestAttCounter'] : '',
                'ivts_SouvenirAttTime' => count($dataIterasi) > 0 ? $dataIterasi[count($dataIterasi) - 1]['ivts_SouvenirAttTime'] : '',
                'ivts_SouvenirAttCounter' => count($dataIterasi) > 0 ? $dataIterasi[count($dataIterasi) - 1]['ivts_SouvenirAttCounter'] : '',
                'ivts_AngpauAttTime' => count($dataIterasi) > 0 ? $dataIterasi[count($dataIterasi) - 1]['ivts_AngpauAttTime'] : '',
                'ivts_AngpauAttCounter' => count($dataIterasi) > 0 ? $dataIterasi[count($dataIterasi) - 1]['ivts_AngpauAttCounter'] : '',
                'ivts_GiftAttTime' => count($dataIterasi) > 0 ? $dataIterasi[count($dataIterasi) - 1]['ivts_GiftAttTime'] : '',
                'ivts_GiftAttCounter' => count($dataIterasi) > 0 ? $dataIterasi[count($dataIterasi) - 1]['ivts_GiftAttCounter'] : '',
            ]);

            if (count($cekUserDoubel) > 1) {
                if ($value['ivts_RsvpStatus'] !== 'Iterasi') {
                    $joinIterasi[] = array_merge($value, [
                        'iterasi_list' => $value['ivts_GuestAtt'] > 0 || $value['ivts_SouvenirAtt'] > 0 || $value['ivts_AngpauAtt'] > 0
                            ? $dataIterasi
                            : []
                    ]);
                }
            } else {
                $joinIterasi[] = array_merge($value, [
                    'iterasi_list' => $value['ivts_GuestAtt'] > 0 || $value['ivts_SouvenirAtt'] > 0 || $value['ivts_AngpauAtt'] > 0
                        ? $dataIterasi
                        : []
                ]);
            }
        }

        return $joinIterasi;
    }

    public function getLatestName(Request $req)
    {
        $select = [
            'inv2.ivts_Id',
            'inv2.ivts_Client_Id',
            'inv2.ivts_Uuid',
            'inv2.ivts_Name',
            'inv2.ivts_GuestAttTime',
            DB::raw('MAX(inv2.ivts_NoHp) as ivts_NoHp'),
            DB::raw('MAX(inv2.ivts_Address) as ivts_Address'),
            DB::raw('MAX(inv2.ivts_Guest) as ivts_Guest'),
            DB::raw("COALESCE(SUM(inv2.ivts_GuestAtt),0) as ivts_GuestAtt"),
            DB::raw('MAX(inv2.ivts_GuestAttCounter) as ivts_GuestAttCounter'),
            DB::raw('MAX(inv2.ivts_Souvenir) as ivts_Souvenir'),
            DB::raw('COALESCE(SUM(inv2.ivts_SouvenirAtt),0) as ivts_SouvenirAtt'),
            DB::raw('MAX(inv2.ivts_SouvenirAttTime) as ivts_SouvenirAttTime'),
            DB::raw('MAX(inv2.ivts_SouvenirAttCounter) as ivts_SouvenirAttCounter'),
            DB::raw('MAX(inv2.ivts_Angpau) as ivts_Angpau'),
            DB::raw('COALESCE(SUM(inv2.ivts_AngpauAtt),0) as ivts_AngpauAtt'),
            DB::raw('MAX(inv2.ivts_AngpauAttTime) as ivts_AngpauAttTime'),
            DB::raw('MAX(inv2.ivts_AngpauAttCounter) as ivts_AngpauAttCounter'),
            DB::raw('MAX(inv2.ivts_Gift) as ivts_GiftOld'),
            DB::raw('MAX(inv2.ivts_GiftDesc) as ivts_Gift'),
            DB::raw('COALESCE(SUM(inv2.ivts_GiftAtt),0) as ivts_GiftAtt'),
            DB::raw('MAX(inv2.ivts_GiftAttTime) as ivts_GiftAttTime'),
            DB::raw('MAX(inv2.ivts_GiftAttCounter) as ivts_GiftAttCounter'),
            DB::raw('MAX(inv.ivts_Category) as ivts_Category'),
            DB::raw('MAX(inv.ivts_Group) as ivts_Group'),
            DB::raw('MAX(inv.ivts_GroupFam) as ivts_GroupFam'),
            DB::raw('MAX(inv.ivts_GroupSub) as ivts_GroupSub'),
            DB::raw('MAX(inv.ivts_Seat) as ivts_Seat'),
            DB::raw('MAX(inv2.ivts_Selfie) as ivts_Selfie'),
            DB::raw('MAX(inv2.ivts_SelfieFile) as ivts_SelfieFile'),
            DB::raw('MAX(inv2.ivts_SelfieTime) as ivts_SelfieTime'),
            DB::raw('MAX(inv2.ivts_capt_Id) as ivts_capt_Id'),
            DB::raw('MAX(inv2.ivts_tplqr_Id) as ivts_tplqr_Id'),
            DB::raw('MAX(inv2.ivts_tplivts_Id) as ivts_tplivts_Id'),
            DB::raw('MAX(inv2.ivts_CounterAtt) as ivts_CounterAtt'),
            DB::raw('MAX(inv2.ivts_Files) as ivts_Files'),
            DB::raw('MAX(inv2.ivts_FilesIvts) as ivts_FilesIvts'),
            DB::raw('MAX(inv2.ivts_RsvpStatus) as ivts_RsvpStatus'),
            DB::raw('MAX(inv2.ivts_RsvpGuest) as ivts_RsvpGuest'),
            DB::raw('MAX(inv2.ivts_RsvpSendCount) as ivts_RsvpSendCount'),
            DB::raw('MAX(inv2.ivts_RsvpMessage) as ivts_RsvpMessage'),
            DB::raw('MAX(inv2.ivts_RsvpTime) as ivts_RsvpTime'),
            DB::raw('MAX(inv2.ivts_LinkExternal) as ivts_LinkExternal'),
            DB::raw('MAX(inv2.ivts_SentTime) as ivts_SentTime'),
            DB::raw('MAX(inv2.ivts_SentStatus) as ivts_SentStatus'),
            DB::raw('MAX(inv2.ivts_Created) as ivts_Created'),
            DB::raw('MAX(inv2.ivts_Updated) as ivts_Updated'),
            DB::raw('MAX(inv2.ivts_Deleted) as ivts_Deleted'),
            DB::raw("
                CASE WHEN (inv2.ivts_RsvpRespond) = 0 THEN 'Tidak Hadir'
                    WHEN (inv2.ivts_RsvpRespond) = 1 THEN CONCAT('Hadir', '(', (inv2.ivts_RsvpGuest), ')')
                    ELSE 'Belum Konfirmasi'
                END AS ivts_RsvpRespond
            ")
        ];

        $data = [];
        if ($req->has('counter') && count($req->counter) > 0) {
            foreach ($req->counter as $key => $value) {
                $data[] =  DB::connection('mysql_db_old')->table('invitations as inv2')->select($select)
                    ->where('inv2.ivts_Client_Id', $req->client_id)
                    ->leftJoin(DB::raw("(
                        SELECT
                            *
                        FROM invitations inv
                        WHERE inv.ivts_RsvpStatus <> 'Iterasi'
                        group by inv.ivts_Uuid
                    ) inv"), function ($j) {
                        $j->on('inv.ivts_Uuid', 'inv2.ivts_Uuid');
                    })
                    ->groupBy(
                        'inv2.ivts_GuestAttTime',
                        'inv2.ivts_Client_Id',
                        'inv2.ivts_Uuid',
                        'inv2.ivts_RsvpRespond',
                        'inv2.ivts_Name'
                    )
                    ->orderBy('inv2.ivts_GuestAttTime', 'desc')
                    ->where("inv2.ivts_GuestAttCounter", "LIKE", $value . "%")
                    ->first();
            }
        }

        $hasil = $data;
        if (count($hasil) > 0) {
            return response()->json([
                'status' => true,
                'label' => 'Data found !',
                'data' => $hasil
            ], 200);
        } else {
            return response()->json([
                'status' => false,
                'label' => 'Data not found !',
                'data' => $hasil
            ], 422);
        }
    }

    public function dynamicFilter($el, $column, $al = '')
    {
        foreach ($column['query_col'] as $key => $valCol) {
            if ($valCol === 'ivts_RsvpRespond') {
                $status = [
                    [
                        'desc' => 'HADIR',
                        'flag' => 1
                    ],
                    [
                        'desc' => 'Tidak Hadir',
                        'flag' => 0
                    ],
                    [
                        'desc' => 'Iterasi / Manual',
                        'flag' => 3
                    ],
                    [
                        'desc' => 'Belum Konfirmasi',
                        'flag' => 2
                    ],
                    [
                        'desc' => 'Belum Konfirmasi',
                        'flag' => 102
                    ],
                    [
                        'desc' => 'Belum Konfirmasi',
                        'flag' => 'NULL'
                    ],
                ];

                $cekDataFilter = array_values(
                    array_filter($status, function ($f) use ($column, $key) {
                        return $f['desc'] == $column['query_val'][$key];
                        // return strpos($f['desc'], $column['query_val'][$key]) !== false;
                    })
                );

                $getValue = [];
                foreach ($cekDataFilter as $keyVal => $valueVal) {
                    $getValue = $valueVal['flag'];
                }

                $valuenya = $getValue;
            } else {
                $valuenya = $column['query_val'][$key];
            }

            if (is_array($valuenya)) {
                $el->whereIn(
                    $al . $valCol,
                    $valuenya
                );
            } else {
                $el->where(
                    $al . $valCol,
                    $column['query_op'][$key],
                    $column['query_op'][$key] === 'like'
                        ? '%' . $valuenya . '%'
                        : $valuenya
                );
            }
        }

        return $el;
    }

    public function getIvts($clientID, $qrCode = '', $cols = [], $status = [], $selectDef = [], $groupBy = [])
    {
        $select = count($selectDef) > 0
            ? $selectDef
            : [
                // 'inv2.ivts_Id',
                'inv2.ivts_Client_Id',
                'inv2.ivts_Uuid',
                'inv2.ivts_Name',
                DB::raw('MAX(inv.ivts_GuestAttTime) as ivts_GuestAttTime'),
                DB::raw('MAX(inv2.ivts_NoHp) as ivts_NoHp'),
                DB::raw('MAX(inv2.ivts_Address) as ivts_Address'),
                DB::raw('MAX(inv2.ivts_Guest) as ivts_Guest'),
                DB::raw("COALESCE(SUM(inv.ivts_GuestAttIter),0) as ivts_GuestAtt"),
                DB::raw('MAX(inv2.ivts_GuestAttCounter) as ivts_GuestAttCounter'),
                DB::raw('MAX(inv2.ivts_Souvenir) as ivts_Souvenir'),
                DB::raw('COALESCE(SUM(inv.ivts_SouvenirAttIter),0) as ivts_SouvenirAtt'),
                DB::raw('MAX(inv.ivts_SouvenirAttTime) as ivts_SouvenirAttTime'),
                DB::raw('MAX(inv2.ivts_SouvenirAttCounter) as ivts_SouvenirAttCounter'),
                DB::raw('MAX(inv2.ivts_Angpau) as ivts_Angpau'),
                DB::raw('COALESCE(SUM(inv.ivts_AngpauAttIter),0) as ivts_AngpauAtt'),
                DB::raw('MAX(inv.ivts_AngpauAttTime) as ivts_AngpauAttTime'),
                DB::raw('MAX(inv2.ivts_AngpauAttCounter) as ivts_AngpauAttCounter'),
                DB::raw('MAX(inv2.ivts_Gift) as ivts_GiftOld'),
                DB::raw('(
                    SELECT ivtsHadiah.ivts_GiftDesc
                    FROM invitations ivtsHadiah
                    where ivtsHadiah.ivts_Client_Id = inv2.ivts_Client_Id
                    and ivtsHadiah.ivts_Uuid = inv2.ivts_Uuid
                    ORDER BY ivtsHadiah.ivts_GuestAttTime DESC
                    LIMIT 1
                ) as ivts_GiftDesc'),
                DB::raw('COALESCE(SUM(inv.ivts_GiftAttIter),0) as ivts_GiftAtt'),
                DB::raw('MAX(inv.ivts_GiftAttTime) as ivts_GiftAttTime'),
                DB::raw('MAX(inv2.ivts_GiftAttCounter) as ivts_GiftAttCounter'),
                DB::raw('MIN(inv2.ivts_Category) as ivts_Category'),
                DB::raw('MAX(inv2.ivts_Group) as ivts_Group'),
                DB::raw('MAX(inv2.ivts_GroupFam) as ivts_GroupFam'),
                DB::raw('MAX(inv2.ivts_GroupSub) as ivts_GroupSub'),
                DB::raw('MAX(inv2.ivts_Seat) as ivts_Seat'),
                DB::raw('MAX(inv2.ivts_Selfie) as ivts_Selfie'),
                DB::raw('MAX(inv2.ivts_SelfieFile) as ivts_SelfieFile'),
                DB::raw('MAX(inv2.ivts_SelfieTime) as ivts_SelfieTime'),
                DB::raw('MAX(inv2.ivts_capt_Id) as ivts_capt_Id'),
                DB::raw('MAX(inv2.ivts_tplqr_Id) as ivts_tplqr_Id'),
                DB::raw('MAX(inv2.ivts_tplivts_Id) as ivts_tplivts_Id'),
                DB::raw('MAX(inv2.ivts_CounterAtt) as ivts_CounterAtt'),
                DB::raw('MAX(inv2.ivts_Files) as ivts_Files'),
                DB::raw('MAX(inv2.ivts_FilesIvts) as ivts_FilesIvts'),
                DB::raw('MAX(inv2.ivts_RsvpStatus) as ivts_RsvpStatus'),
                DB::raw('MAX(inv2.ivts_RsvpGuest) as ivts_RsvpGuest'),
                DB::raw('MAX(inv2.ivts_RsvpSendCount) as ivts_RsvpSendCount'),
                DB::raw('MAX(inv2.ivts_RsvpMessage) as ivts_RsvpMessage'),
                DB::raw('MAX(inv2.ivts_RsvpTime) as ivts_RsvpTime'),
                DB::raw('MAX(inv2.ivts_LinkExternal) as ivts_LinkExternal'),
                DB::raw('MAX(inv2.ivts_SentTime) as ivts_SentTime'),
                DB::raw('MAX(inv2.ivts_SentStatus) as ivts_SentStatus'),
                DB::raw('MAX(inv2.ivts_Created) as ivts_Created'),
                DB::raw('MAX(inv2.ivts_Updated) as ivts_Updated'),
                DB::raw('MAX(inv2.ivts_Deleted) as ivts_Deleted'),
                DB::raw('MAX(inv2.ivts_Anggota) as ivts_Anggota'),
                DB::raw("
                CASE WHEN (inv2.ivts_RsvpRespond) = 0 THEN 'Tidak Hadir'
                    WHEN (inv2.ivts_RsvpRespond) = 1 THEN CONCAT('Hadir', '(', (inv2.ivts_RsvpGuest), ')')
                    ELSE 'Belum Konfirmasi'
                END AS ivts_RsvpRespond
            ")
            ];

        $data = DB::connection('mysql_db_old')->table('invitations as inv2')->select($select)
            ->where('inv2.ivts_Client_Id', $clientID)
            ->leftJoin(DB::raw("(
                SELECT
                    inv.ivts_Uuid,
                    COALESCE(SUM(ivts_GuestAtt),0) as ivts_GuestAttIter,
                    MAX(ivts_GuestAttTime) as ivts_GuestAttTime,
                    COALESCE(SUM(ivts_SouvenirAtt),0) as ivts_SouvenirAttIter,
                    MAX(ivts_SouvenirAttTime) as ivts_SouvenirAttTime,
                    COALESCE(SUM(ivts_AngpauAtt),0) as ivts_AngpauAttIter,
                    MAX(ivts_AngpauAttTime) as ivts_AngpauAttTime,
                    COALESCE(SUM(ivts_GiftAtt),0) as ivts_GiftAttIter,
                    MAX(ivts_GiftAttTime) as ivts_GiftAttTime,
                    MAX(ivts_GiftDesc) AS ivts_GiftDesc
                FROM invitations inv
                WHERE inv.ivts_RsvpStatus = 'Iterasi'
                group by inv.ivts_Uuid
            ) inv"), function ($j) {
                $j->on('inv.ivts_Uuid', 'inv2.ivts_Uuid');
            })
            ->groupBy(
                count($groupBy) > 0
                    ? $groupBy
                    : [
                        'inv2.ivts_Client_Id',
                        'inv2.ivts_Uuid',
                        // 'inv2.ivts_RsvpRespond',
                        'inv2.ivts_Name'
                    ]
            )
            ->orderBy('inv.ivts_GuestAttTime', 'desc');

        if (count($status) > 0) {
            $data->whereIn('inv2.ivts_RsvpStatus', $status);
        }

        if (!empty($qrCode)) {
            $data->where('inv2.ivts_Uuid', $qrCode);
        }

        if (!empty($cols) && count($cols['query_col']) > 0) {
            $data = $this->dynamicFilter($data, $cols, 'inv2.');
        }

        return $data;
    }
}
