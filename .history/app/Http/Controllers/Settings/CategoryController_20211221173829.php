<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

// Model
use App\Models\Settings\Category;
use App\Models\Settings\UserCategory;

class CategoryController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'cat_name' => 'required|alpha_dash|unique:rsvp_category_mstr,cat_name'
        ]);

        try {
            $insert = Category::updateOrCreate([
                'cat_name' => $request->cat_name
            ], $request->all());

            return [
                'status' => true,
                'label' => 'Category successfully inserted !',
                'data' => $insert
            ];
        } catch (\Throwable $th) {
            return response()->json( [
                'status' => false,
                'label' => $th->getMessage(),
                'data' => [
                    'entity' => 'rsvp_category_mstr',
                    'action' => 'create',
                    'result' => $th->getMessage()
                ]
            ], 409);
        }
    }

    public function storeUserCategory(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'category_id' => 'required|array|check_array:1'
        ]);

        try {
            $hasil = [];
            foreach ($request->category_id as $key => $value) {
                $cekData = UserCategory::find($value);

                return $cekData;
                if(!$cekData->categoryMaster->exists()){
                    $hasil[] = UserCategory::create([
                        'username' => $request->username,
                        'category_id' => $value
                    ]);
                } else {
                    $hasil[] =[
                        'status' => false,
                        'label' => 'No category ID found !'
                    ];
                }
            }

            return [
                'status' => true,
                'label' => 'Inserted success',
                'data' => $hasil
            ];
        } catch (\Throwable $th) {
            return response()->json( [
                'status' => false,
                'label' => $th->getMessage(),
                'data' => [
                    'entity' => 'rsvp_users_cat',
                    'action' => 'create',
                    'result' => $th->getMessage()
                ]
            ], 409);
        }
    }
}
