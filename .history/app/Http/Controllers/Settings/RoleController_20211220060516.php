<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

// Models
use App\Models\Settings\Role;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login','register']]);
    }

    public function store(Request $req)
    {
        $this->validate($req, [
            'roles_name' => 'required|unique:rsvp_roles_mstr,roles_name'
        ]);

        try {
            $insert = Role::create($req);

            return [
                'status' => true,
                'label' => 'Menu successfully inserted !',
                'data' => $insert
            ];
        } catch (\Throwable $th) {
            return response()->json( [
                'entity' => 'roles',
                'action' => 'create',
                'result' => $th->getMessage()
            ], 409);
        }
    }
}
