<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserDet;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login','register']]);
    }

    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function register(Request $request)
    {
        // return 'test';
        //validate incoming request
        $this->validate($request, [
            'username' => 'required|string|unique:users',
            'password' => 'required|confirmed',
        ]);

        try
        {
            $user = new User;
            $user->username= $request->input('username');
            $user->password = app('hash')->make($request->input('password'));
            $user->save();

            return response()->json( [
                        'entity' => 'users',
                        'action' => 'create',
                        'result' => 'success'
            ], 201);

        }
        catch (\Exception $e)
        {
            return response()->json( [
                       'entity' => 'users',
                       'action' => 'create',
                       'result' => 'failed'
            ], 409);
        }
    }

    public function registerTamu(Request $request)
    {
        $cekIDTamu = User::with('userRole', function ($q) {
            $q->where('roles_id', 4);
        })->has('userRole', function ($q) {
            $q->where('roles_id', 4);
        })->first();

        $genUserName = substr(md5(microtime()), 0, 10);

        $insertUsername = $this->register(new Request([
            'username' => $genUserName,
            'password' => env('RSVP_SECRET')
        ]))
    }

     /**
     * Get a JWT via given credentials.
     *
     * @param  Request  $request
     * @return Response
     */
    public function login(Request $request)
    {
          //validate incoming request
        $this->validate($request, [
            'username' => 'required|string',
            'password' => 'required|string',
        ]);

        $credentials = $request->only(['username', 'password']);

        if (! $token = Auth::attempt($credentials)) {
            return response()->json(['message' => 'Unauthorized'], 401);
        }
        return $this->respondWithToken($token);
    }

     /**
     * Get user details.
     *
     * @param  Request  $request
     * @return Response
     */
    public function me()
    {
        return response()->json(auth()->user());
    }
}
