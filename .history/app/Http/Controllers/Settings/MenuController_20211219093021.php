<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

// Models
use App\Models\Settings\Menu;

// Requests
use App\Http\Requests\Settings\MenuRequest;

class MenuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login','register']]);
    }

    public function store(MenuRequest $req)
    {
        $getID = Menu::orderBy('menus_id', 'desc')->first();

        if (empty($getID)) {
            $id = 'A0001';
        } else {
            $id = sprintf('%04d', (int) substr($getID->menus_id, -3) + 1);
        }

        $insert = Menu::create([
            'menus_id' => $id,
            'menus_name' => $req->menus_name,
            'menus_url' => $req->menus_url,
        ]);

        return [
            'status' => true,
            'label' => 'Menu successfully inserted !',
            'data' => $insert
        ];
    }
}
