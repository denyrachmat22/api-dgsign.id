<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserDet;

use App\Http\Controllers\Settings\RoleController;

class AuthController extends RoleController
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login','register', 'registerTamu', 'loginViaQR']]);
    }

    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function register(Request $request)
    {
        // return 'test';
        //validate incoming request
        $this->validate($request, [
            'username' => 'required|string|unique:users',
            'password' => 'required|confirmed',
        ]);

        try
        {
            $user = new User;
            $user->username= $request->input('username');
            $user->password = app('hash')->make($request->input('password'));
            $user->save();

            return [
                'status' => true,
                'label' => 'User successfully created !!',
                'data' => $user
            ];

        }
        catch (\Exception $th)
        {
            return response()->json( [
                'status' => false,
                'label' => $th->getMessage(),
                'data' => [
                    'entity' => 'users',
                    'action' => 'create',
                    'result' => $th->getMessage()
                ]
            ], 409);
        }
    }

    public function updateUserDetail(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|unique:rsvp_users_det'
        ]);

        try {
            $insert = UserDet::updateOrCreate([
                'username' => $request->username
            ],$request->all());

            return [
                'status' => true,
                'label' => 'Menu successfully inserted !',
                'data' => $insert
            ];
        } catch (\Throwable $th) {
            return response()->json( [
                'status' => false,
                'label' => $th->getMessage(),
                'data' => [
                    'entity' => 'rsvp_users_det',
                    'action' => 'create',
                    'result' => $th->getMessage()
                ]
            ], 409);
        }
    }

    public function registerTamu(Request $request)
    {
        $genUserName = substr(md5(microtime()), 0, 10);

        $insertUsername = $this->register(new Request([
            'username' => $genUserName,
            'password' => env('RSVP_SECRET'),
            'password_confirmation' => env('RSVP_SECRET')
        ]));

        if ($insertUsername['status']) {
            try {
                $insertDet = $this->updateUserDetail(new Request([
                    'username' => $genUserName,
                    'fullname' => $request->fullname,
                    'users_det_addr1' => $request->users_det_addr1,
                    'users_det_addr2' => $request->users_det_addr2,
                    'users_det_phone' => $request->users_det_phone,
                    'users_det_fam_count' => $request->users_det_fam_count,
                    'users_det_souvenir_count' => $request->users_det_souvenir_count,
                    'users_det_angpau_count' => $request->users_det_angpau_count,
                ]));

                if ($insertDet['status']) {
                    $insertRoles = $this->storeUserMapping(new Request([
                        'username' => $genUserName,
                        'roles_id' => 4
                    ]));

                    return [
                        'status' => true,
                        'label' => 'User Detail, user master, user roles successfully inserted !',
                        'data' => $insertRoles
                    ];
                }
            } catch (\Throwable $th2) {
                return response()->json( [
                    'status' => false,
                    'label' => $th2->getMessage(),
                    'data' => [
                        'entity' => 'rsvp_users_det',
                        'action' => 'create',
                        'result' => $th2->getMessage()
                    ]
                ], 409);
            }
        } else {
            return response()->json( [
                'status' => false,
                'label' => $insertUsername->label,
                'data' => $insertUsername->data
            ], 409);
        }
    }

     /**
     * @OA\Post(
     *     path="https://api.dgsign.id/api/login",
     *     operationId="/sample/category/things",
     *     tags={"Auth"},
     *     @OA\Parameter(
     *         name="username",
     *         in="query",
     *         description="Username user for login",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="password",
     *         in="query",
     *         description="Password user for login",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Returns authorization token for login token",
     *         @OA\JsonContent(
     *              type="object",
     *                  status=true,
     *                  @OA\Property(property="property1", type="number")
     *         )
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Error: username or password not right",
     *     ),
     * )
     */
    public function login(Request $request)
    {
        //validate incoming request
        $this->validate($request, [
            'username' => 'required|string',
            'password' => 'required|string',
        ]);

        $credentials = $request->only(['username', 'password']);

        if (! $token = Auth::attempt($credentials)) {
            return response()->json([
                'status' => false,
                'message' => 'Unauthorized'
            ], 401);
        }

        $dataUser = User::where('username', $request->username)->first();
        return array_merge(
            [
                'status' => true,
                'data' => $dataUser->userDetail
            ],
            $this->respondWithToken($token)->original
        );
    }

    public function loginViaQR($qrID)
    {
        $cekLogin = $this->login(new Request([
            'username' => base64_decode($qrID),
            'password' => env('RSVP_SECRET')
        ]));

        return $cekLogin;
    }

     /**
     * Get user details.
     *
     * @param  Request  $request
     * @return Response
     */
    public function me()
    {
        return response()->json(auth()->user());
    }
}
